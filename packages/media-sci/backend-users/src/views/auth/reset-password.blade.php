@extends('BackendUsers::auth.layout')
@section('content')

<h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>Easily Using</span></h6>
<div class="card-body pt-0">
	@errors
	<form class="form-horizontal" method="POST">
        @csrf
        <fieldset class="form-group floating-label-form-group">
            <label for="user-name">Password</label>
            <input type="password" name="password" class="form-control" id="user-name" placeholder="new password" required>
        </fieldset>
        <fieldset class="form-group floating-label-form-group">
            <label for="user-name">Re-password</label>
            <input type="password" name="password_confirmation" class="form-control" id="user-name" placeholder="repeate password" required>
        </fieldset>
        <button type="submit" class="btn btn-outline-primary btn-block"><i class="ft-unlock"></i>Reset Password</button>
        <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>Back to login</span></h6>
        <div class="form-group row">
            <div class="col-12">
                <a href="{{route('login')}}">Login</a>
            </div>
        </div>    
	</form>
</div>	
@endsection