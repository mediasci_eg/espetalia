# Backend Posts

# Installation

1- Define package to your composer.json add this lines to composer.json :
` "repositories": [
    {
      "type": "vcs",
      "url":  "https://elsayed_nofal:5pGrh4AP3UEJdWCA6GR4@bitbucket.org/elsayed_nofal/media-manager.git"
    }],`

    
2- Run command `composer require elsayed_nofal/media-manager:dev-master`

3- add service provider to config/app.php
`Elsayednofal\MediaManager\MediaManagerServiceProvider::class,`

4- add alias to config/app.php
`'Media' => Elsayednofal\MediaManager\Controllers\MediaController::class,`
if you use ImageManager befor 
add 
`'ImageManager' => Elsayednofal\MediaManager\Controllers\MediaController::class,`

6- Run command `php artisan vendor:publish`

7 -Run command `php artisan migrate`

# configration    
	- edit configration from file config/media-manager
	- you can change view from backend/media-manager

# usage

1- add `{!! ImageManager::loadAssets() !!}` befor </body> close
2- add `  {!! ImageManager::loadModal() !!}` after </body>

    - basic :  `{!! Media::selector('images[]') !!}`
       
    - on update : `{!! Media::selector('images[]',[1,2]) !!}`

    - single image choose : `{!! Media::selector('images[]',[],false) !!}`

    - target category : `{!! Media::selector('images[]',[],false,'products') !!}` or {!! Media::selector('images[]',[],false,5) !!}

    - Retrive Image (display image ): `{!! ImageManager::getImagePath(5,'small')!!}`

    
    
    
