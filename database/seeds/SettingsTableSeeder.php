<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('settings')->insert(
        array(
            'facebook_link' => 'https://www.facebook.com/',
            'twitter_link' => 'https://www.twitter.com/',
            'linked_in_link' => 'https://www.linked-in.com/',
            'address' => 'address'
        )
    );
    }
}
