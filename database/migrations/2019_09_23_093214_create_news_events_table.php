<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewsEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news_events', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title', 191);
			$table->text('content', 65535);
			$table->string('slug', 191);
			$table->string('image', 191);
			$table->date('date');
			$table->boolean('is_featured')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news_events');
	}

}
