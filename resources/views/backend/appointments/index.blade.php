@extends(config('backend-users.backend_layout'))

@section(config('backend-users.layout_content_area'))
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h3 class="content-header-title">Appointments Data</h3>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="./backend">Home</a>
                </li>
                <li class="breadcrumb-item active">Appointments
                </li>
            </ol>
        </div>
    </div>
</div>

<div class="content-body">
    <section id="search">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Find a Patient <i class="fa fa-search" aria-hidden="true"></i></h3>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form>
                                <div class="row">
                                    <div class="col-1">
                                        <div class="form-group">
                                            <label for="id">#</label>
                                            <input type="number" min="1" name="user[id]" value="{{request()->input('user.id')}}" id="id" class="form-control" placeholder="id" aria-describedby="helpId">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" name="user[full_name]" value="{{request()->input('user.full_name')}}" id="name" class="form-control" placeholder="Name" aria-describedby="helpId">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="phone">Phone</label>
                                            <input type="text" name="user[phone]" value="{{request()->input('user.phone')}}" id="email" class="form-control" placeholder="Phone" aria-describedby="helpId">
                                        </div>
                                    </div>
                                    <div class="col-1">
                                        <div class="form-group">
                                            <br>
                                            <input type="hidden" name="search" value="1">
                                            <button type="submit" title="search" class="btn btn-outline-primary btn-xs"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>      
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </section>
    <section id="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">

                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div class="table-responsive">
                                <button type="button"  title="Trashed Appointments" class="btn btn-outline-danger" data-toggle="modal" data-target="#yourModal">View Trashed Appointments</button>
                                <div class="modal fade" id="yourModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Close</h4>
                                            </div>
                                            @foreach($trashed as $trash)
                                            <div class="modal-body">
                                                Name :       {{$trash->full_name}}
                                                <br>
                                                Date :       {{$trash->date}}
                                                <hr>
                                            </div>
                                            @endforeach
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <table class="table table-striped table-bordered table-hover example1">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Mobile number</th>
                                            <th>Department</th>
                                            <th>Date</th>
                                            <th>Reply</th>
                                            <th>Creation Time</th>
                                            <th>View All</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($appointments as $appointment)
                                        <tr>
                                            <td>{{$appointment->full_name}}</td>
                                            <td>{{$appointment->phone}}</td>
                                            <td>{{$appointment->department->name}}</td>
                                            <td>{{date('d-M-y', strtotime($appointment->date)) }}</td>
                                            <td><input id="{{$appointment->id}}" type="checkbox" name="is_reply" @if($appointment->is_reply == 1) checked @endif></td>
                                            <td>{{$appointment->created_at}}</td>
                                            <td>
                                                <a href="Javascript:void(0)" class="btn btn-outline-primary btn-xs" data-toggle="modal" title="View Details" data-target="#exampleModal{{$appointment->id}}">
                                                    <i class="fa fa-list" aria-hidden="true"></i>
                                                </a>
                                                <a href="backend/appointments/delete/{{$appointment->id}}" class="btn btn-xs btn-outline-danger delete" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                            </td>
                                    <div class="modal fade" id="exampleModal{{$appointment->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel"> All Patients Data</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Name :       {{$appointment->full_name}}
                                                    <hr>
                                                    Phone :       {{$appointment->phone}}
                                                    <hr>
                                                    Department Name :       {{$appointment->department->name}}
                                                    <hr>
                                                    Date :       {{$appointment->date}}
                                                    <hr>
                                                    Reply :      @if($appointment->is_reply == 1 )Replied @else Not Replied @endif
                                                    <hr>
                                                    created_at :       {{$appointment->created_at}}
                                                    <hr>
                                                </div>
                                                {{-- <div class="modal-footer">
                                                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                  <button type="button" class="btn btn-primary">Save changes</button>
                                                                </div> --}}
                                            </div>
                                        </div>
                                    </div>                                    

                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $appointments->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@push('script')
@deletejs
<script>
    $(document).ready(function () {
        $("input:checkbox").change(function () {
            var reply_id = $(this).attr('id');

            $.ajax({
                type: 'POST',
                url: "<?= url('/backend/appointments/change-status') ?>",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                data: {"reply_id": reply_id}
            });
        });
    });
    </script>
    @endpush