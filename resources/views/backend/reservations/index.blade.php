@extends(config('backend-users.backend_layout'))

@section(config('backend-users.layout_content_area'))
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h3 class="content-header-title">Reservations Data</h3>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="./backend">Home</a>
                </li>
                <li class="breadcrumb-item active">Reservations
                </li>
            </ol>
        </div>
    </div>
</div>

<div class="content-body">
    <section id="search">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Find a Patient <i class="fa fa-search" aria-hidden="true"></i></h3>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form>
                                <div class="row">
                                    <div class="col-1">
                                        <div class="form-group">
                                            <label for="id">#</label>
                                            <input type="number" min="1" name="user[id]" value="{{request()->input('user.id')}}" id="id" class="form-control" placeholder="id" aria-describedby="helpId">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" name="user[name]" value="{{request()->input('user.name')}}" id="name" class="form-control" placeholder="Name" aria-describedby="helpId">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text" name="user[email]" value="{{request()->input('user.email')}}" id="email" class="form-control" placeholder="email" aria-describedby="helpId">
                                        </div>
                                    </div>
                                    <div class="col-1">
                                        <div class="form-group">
                                            <br>
                                            <input type="hidden" name="search" value="1">
                                            <button type="submit" title="search" class="btn btn-outline-primary btn-xs"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>      
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </section>
    <section id="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">

                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <button type="button"  title="Trashed Appointments"  class="btn btn-outline-danger" data-toggle="modal" data-target="#trashModal">View Trashed Reservations</button>
                            <div class="table-responsive">
                                <div class="modal fade" id="trashModal" tabindex="-1" role="dialog" aria-labelledby="trashModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Close</h4>
                                            </div>
                                            @foreach($trashed as $trash)
                                            <div class="modal-body">
                                                Name :       {{$trash->name}}
                                                <br>
                                                Date :       {{$trash->created_at}}
                                                <hr>
                                            </div>
                                            @endforeach
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <table class="table table-striped table-bordered table-hover example1">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Mobile number</th>
                                            <th>Doctor Name</th>
                                            <th>Day</th>
                                            <th>Time</th>
                                            <th>Replied</th>
                                            <th>Creation Time</th>
                                            <th>View All</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($applicants as $applicant)
                                        <tr>
                                            <td>{{$applicant->name}}</td>
                                            <td>{{$applicant->phone}}</td>
                                            <td>{{$applicant->doctor->name}}</td>
                                            <td>{{$applicant->date}}</td>
                                            <td>{{$applicant->time}}</td>
                                            <td><input id="{{$applicant->id}}" type="checkbox" name="is_reply" @if($applicant->is_reply == 1) checked @endif></td>
                                            <td>{{$applicant->created_at}}</td>
                                            <td>
                                                <a href="Javascript:void(0)" class="btn btn-outline-primary btn-xs" data-toggle="modal" title="View Details" data-target="#exampleModal{{$applicant->id}}">
                                                    <i class="fa fa-list" aria-hidden="true"></i>
                                                </a>
                                                <a href="backend/reservations/delete/{{$applicant->id}}" class="btn btn-xs btn-outline-danger delete" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                            </td>
                                    <div class="modal fade" id="exampleModal{{$applicant->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel"> All Applicants Data</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Name :       {{$applicant->name}}
                                                    <hr>
                                                    Email :       {{$applicant->email}}
                                                    <hr>
                                                    Phone :       {{$applicant->phone}}
                                                    <hr>
                                                    Message :       {{$applicant->message}}
                                                    <hr>
                                                    Doctor Name :       {{$applicant->doctor->name}}
                                                    <hr>
                                                    Reply :       @if($applicant->is_reply == 1 )Replied @else Not Replied @endif
                                                    <hr>
                                                    created_at :       {{$applicant->created_at}}
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $applicants->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@push('script')
@deletejs
<script>
    $(document).ready(function () {
        $("input:checkbox").change(function () {
            var reply_id = $(this).attr('id');

            $.ajax({
                type: 'POST',
                url: "<?= url('/backend/reservations/change-status') ?>",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                data: {"reply_id": reply_id}
            });
        });
    });
</script>
@endpush