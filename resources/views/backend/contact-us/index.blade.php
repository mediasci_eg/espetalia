@extends(config('backend-users.backend_layout'))

@section(config('backend-users.layout_content_area'))
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h3 class="content-header-title">Contact Us Data</h3>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="./backend">Home</a>
                </li>
                <li class="breadcrumb-item active">Contact Us
                </li>
            </ol>
        </div>
    </div>
</div>

<div class="content-body">
    <section id="search">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Find a Contact <i class="fa fa-search" aria-hidden="true"></i></h3>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form>
                                <div class="row">
                                    <div class="col-1">
                                        <div class="form-group">
                                            <label for="id">#</label>
                                            <input type="number" min="1" name="user[id]" value="{{request()->input('user.id')}}" id="id" class="form-control" placeholder="id" aria-describedby="helpId">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" name="user[name]" value="{{request()->input('user.name')}}" id="name" class="form-control" placeholder="Name" aria-describedby="helpId">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="phone">Phone</label>
                                            <input type="text" name="user[phone]" value="{{request()->input('user.phone')}}" id="email" class="form-control" placeholder="Phone" aria-describedby="helpId">
                                        </div>
                                    </div>
                                    <div class="col-1">
                                        <div class="form-group">
                                            <br>
                                            <input type="hidden" name="search" value="1">
                                            <button type="submit" title="search" class="btn btn-outline-primary btn-xs"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>      
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </section>
    <section id="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">

                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <button type="button"  title="Trashed Contacts"  class="btn btn-outline-danger" data-toggle="modal" data-target="#trashModal">View Trashed Contacts</button>
                            <div class="table-responsive">
                                <div class="modal fade" id="trashModal" tabindex="-1" role="dialog" aria-labelledby="trashModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Close</h4>
                                            </div>
                                            @foreach($trashed as $trash)
                                            <div class="modal-body">
                                                Name :       {{$trash->name}}
                                                <br>
                                                Date :       {{$trash->created_at}}
                                                <hr>
                                            </div>
                                            @endforeach
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <table class="table table-striped table-bordered table-hover example1">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Mobile number</th>
                                            <th>Mail</th>
                                            <th>Replied</th>
                                            <th>Creation Time</th>
                                            <th>View All</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($contacts as $contact)
                                        <tr>
                                            <td>{{$contact->name}}</td>
                                            <td>{{$contact->phone}}</td>
                                            <td>{{$contact->mail}}</td>
                                            <td>
                                                @if($contact->is_reply == 0) 
                                                <div class="d-none" id="p-replied{{$contact->id}}">Replied</div>
                                                <div class="col-1" id="after-send{{$contact->id}}">
                                                    <div class="form-group"> 
                                                        <br>
                                                                        <button type="button"  title="Send Reply" class="btn btn-default"  data-toggle="modal" data-target="#replyModal{{$contact->id}}">Send Reply</button>
                                                                        <div class="modal fade" id="replyModal{{$contact->id}}" tabindex="-1" role="dialog" aria-labelledby="replyModalLabel">
                                                                            <div class="modal-dialog" role="document">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        <h4 class="modal-title" id="replyModalLabel">Close</h4>
                                                                                                                                                   </div>
                                                                                    <div class="modal-body">
                                                                        <input type="text" name="id" id="contact_id" value="{{$contact->id}}" hidden="true" class="form-control">
                                                                                                <?php
                                                                                                $subject = '';
                                                                                                $message = '';
                                                                                                ?>
                                                                                                <div class="form-group">
                                                                                                    <label for="subject">Subject: </label>
                                                                                                    <input type="text" id="contact_subject" name="subject" required class="form-control">
                                                                                                </div>
                                                                                                <div class="form-group">
                                                                                                    <label for="subject">Message: </label>
                                                                                                    <textarea name="message" id="contact_message" class="form-control" cols="50" rows="3" required></textarea>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="modal-footer">
                                                                                                <button type="submit" title="send" data-id="{{$contact->id}}" class="btn btn-default send_reply" data-dismiss="modal">Send</button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                    </div>
                                                                    @else Replied @endif
                                                                    </td>




                                                                    <td>{{date('d-M-y', strtotime($contact->created_at)) }}</td>
                                                                    <td>
                                                                        <a href="Javascript:void(0)" class="btn btn-outline-primary btn-xs" data-toggle="modal" title="View Details" data-target="#exampleModal{{$contact->id}}">
                                                                            <i class="fa fa-list" aria-hidden="true"></i>
                                                                        </a>
                                                                        <a href="backend/contact-us/delete/{{$contact->id}}" class="btn btn-xs btn-outline-danger delete" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                                    </td>
                                                                    <div class="modal fade" id="exampleModal{{$contact->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog" role="document">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <h5 class="modal-title" id="exampleModalLabel"> All Contacts Data</h5>
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    Name :       {{$contact->name}}
                                                                                    <hr>
                                                                                    Phone :       {{$contact->phone}}
                                                                                    <hr>
                                                                                    Mail :       {{$contact->mail}}
                                                                                    <hr>
                                                                                    Subject :       {{$contact->subject}}
                                                                                    <hr>
                                                                                    Message :       {{$contact->message}}
                                                                                    <hr>
                                                                                    Reply :       @if($contact->is_reply == 1 )Replied @else Not Replied @endif
                                                                                    <hr>
                                                                                    created_at :       {{date('d-M-y', strtotime($contact->created_at)) }}
                                                                                    <hr>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>                                    

                                                                    </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                    </table>
                                                                    {{ $contacts->links() }}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                                </section>
                                                </div>
                                                @endsection
                                                @push('script')
                                                @deletejs

                                                <script>
                                                    $(document).ready(function () {
                                                        $(".send_reply").click(function (e) {
                                                            var me = $(this);
                                                            var id = me.data("id");
                                                            e.preventDefault();
                                                            var contact_id = $('#contact_id').val();
                                                            var contact_subject = $('#contact_subject').val();
                                                            var contact_message = $('#contact_message').val();
                                                            $.ajax({
                                                                type: 'POST',
                                                                url: "<?= url('/backend/contact-us/send') ?>",
                                                                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                                                                data: {"contact_id": contact_id,
                                                                    "contact_subject": contact_subject,
                                                                    "contact_message": contact_message
                                                                },
                                                                success: function (response) {
                                                                    console.log('dnj');
                                                                    $('#after-send' + id).hide();
                                                                    //                    $('#p-replied'+id).text(JSON.parse(response).replied);
                                                                    $('#p-replied' + id).removeClass("d-none");
                                                                    //                    
                                                                }
                                                            });
                                                        });
                                                    });
                                                </script>
                                                @endpush