<?php

use App\Http\Controllers\Helpers\Upload;

if (!isset($model))
    $model = new \App\Models\Slider;
?>
<div class="card">
    <div class="card-content">
        <div class="card-body">
            @success
            @errors
            <form action="backend/sliders/store" method="post" id="bu-form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$model->id}}">
                <div class="row">
                    <div class="col-5">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" value="{{old('name' , $model->name )}}" id="name" class="form-control" required>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <label for="title">Link</label>
                            <input type="text" name="link" value="{{old('link' , $model->link )}}" id="link" class="form-control">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="title">content</label>
                            <textarea name="content" class="form-control" cols="50" rows="3">{{$model->content}}</textarea>
                        </div>
                    </div>
                    
                    <div class="col-5">
                        <div class="form-group">
                            <label class="control-label">Image</label>
                            <input type="file" class='form-control' value="{{$model->image}}" name="image">
                            @if($model->image !='')
                            <img src='{{url('uploads/'.$model->image)}}' style='width:200px'>
                            @endif
                        </div>
                    </div>

                </div>
                <hr>
                <div class="row">
                    <button type="submit" class="btn btn-xs btn-primary">Save <i class="fas fa-save"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('script')

@endpush