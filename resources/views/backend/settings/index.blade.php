@extends(config('backend-users.backend_layout'))

@section(config('backend-users.layout_content_area'))

<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h3 class="content-header-title">Settings Data</h3>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="./backend">Home</a>
                </li>
                <li class="breadcrumb-item active">Settings
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
    <form action="backend/settings/update_settings" method="post" class="form-horizontal" id="form2">
        @csrf
        <?php foreach ($settings as $value) { ?>

            <?php if ($value->type == 1) { ?>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="control-label"><?php $string = str_replace('_', ' ', $value->key);
        echo ucwords($string); ?></label>
                        <input type="text" name="value[<?= $value->id; ?>]" placeholder="Please enter <?php $string = str_replace('_', ' ', $value->key);
        echo ucwords($string); ?>" class="form-control" value="<?= $value->value; ?>"  />
                    </div>
                </div>
    <?php } ?>
    <?php if ($value->type == 2) { ?>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="control-label"><?php $string = str_replace('_', ' ', $value->key);
        echo ucwords($string); ?></label>
                        <input type="checkbox" name="value[<?= $value->id; ?>]"  value="<?= $value->value; ?>" checked />
                    </div>
                </div>
    <?php } ?>
    <?php if ($value->type == 3) { ?>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="control-label"><?php $string = str_replace('_', ' ', $value->key);
        echo ucwords($string); ?></label>
                        <textarea name="value[<?= $value->id; ?>]" class="form-control" rows="3" placeholder="Please enter <?php $string = str_replace('_', ' ', $value->key);
        echo ucwords($string); ?>"><?= $value->value; ?></textarea>
                    </div>
                </div>
    <?php } ?>

<?php } ?>

        <div class="hr-line-dashed"></div>
        <div class="form-group">
            <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save Changes" />
        </div>

    </form>
</div>

@endsection
@push('script')
@deletejs
@endpush