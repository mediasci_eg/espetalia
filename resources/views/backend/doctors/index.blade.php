@extends(config('backend-users.backend_layout'))

@section(config('backend-users.layout_content_area'))
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-1">
        <h3 class="content-header-title">Doctors Data</h3>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="./backend">Home</a>
                </li>
                <li class="breadcrumb-item active">Doctors
                </li>
            </ol>
        </div>
    </div>
</div>

<div class="content-body">
    <section id="search">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Find a Doctor <i class="fa fa-search" aria-hidden="true"></i></h3>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form>
                                <div class="row">

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <!--<input type="text" name="title" value="{{request()->input('news_events.title')}}" id="name" class="form-control" placeholder="Name" aria-describedby="helpId">-->
                                            <input type="text" name="name" value="{{request()->input('doctors.name')}}" placeholder="Enter Name" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-1">
                                        <div class="form-group">
                                            <br>
                                            <!--<input type="hidden" name="search" value="1">-->
                                            <button type="submit" title="search" class="btn btn-outline-primary btn-xs"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>    
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </section>
    <section id="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Doctors</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <a href="backend/doctors/create" class="btn btn-xs btn-outline-primary" title="new Doctor"><i class="fa fa-plus" aria-hidden="true"></i> New</a>
                            <br><br>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>

                                            <th>Name</th>
                                            <th>Title</th>
                                            <!--<th>Department</th>-->
                                            <th>Image</th>
                                            <th>Creation Time</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($result as $raw)
                                        <tr>
                                            <td>{{$raw->name}}</td>
                                            <td>{{$raw->title}}</td>
                                            <!--<td>{{@$raw->department->name}}</td>-->
                                            <td><img src='{{url('uploads/'.$raw->image)}}' style='width:50px'></td>
                                            <td>{{$raw->created_at}}</td>
                                            <td>
                                                <a href="backend/doctors/update/{{$raw->id}}" class="btn btn-sm btn-outline-info" title="Edit"><i class="fa fa-pen" aria-hidden="true"></i></a>
                                                <a href="backend/doctors/delete/{{$raw->id}}" class="btn btn-sm btn-outline-danger delete" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $result->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@push('script')
@deletejs
@endpush