<?php

use App\Http\Controllers\Helpers\Upload;

if (!isset($model))
    $model = new \App\Models\Doctor;
?>
<div class="card">
    <div class="card-content">
        <div class="card-body">
            @success
            @errors
            <form action="backend/doctors/store" method="post" id="bu-form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$model->id}}">
                <div class="row">
                    <div class="col-5">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" value="{{old('name' , $model->name )}}" id="name" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-7">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" value="{{old('title' , $model->title )}}" id="name" class="form-control" required>
                        </div>
                    </div>

                    <!--<div class="col-6">
                        <div class="form-group">
                            <label for="name">Department</label>
                            <select name="department_id" class="form-control">
                                <option value="" selected disabled hidden>Choose Your Department</option>
                                foreach($departments as $department)
                                <option value="$department->id" if($department->id==$model->department_id) selected endif>$department->name</option>
                                endforeach
                            </select>
                        </div>
                    </div>-->

                    <div class="col-12">
                        <div class="form-group">
                            <label class="control-label">Content</label>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css() ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('content', 'content', $model->content) ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js() ?>
                        </div>
                    </div>

                    <div class="col-5">
                        <div class="form-group">
                            <label class="control-label">Image</label>
                            <input type="file" class='form-control' value="{{$model->image}}" name="image">
                            @if($model->image !='')
                            <img src='{{url('uploads/'.$model->image)}}' style='width:200px'>
                            @endif
                        </div>
                    </div>

                </div>
                <hr>
                <div class="row">
                    <button type="submit" class="btn btn-xs btn-primary">Save <i class="fas fa-save"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('script')
<script>
    $(document).ready(function () {
        $('.type').change(function () {
            var val = $(this).val();
            $('.event-detials , .news-detials').addClass('d-none');
            if (val == 'news')
                $('.news-detials').removeClass('d-none');
            else
                $('.event-detials').removeClass('d-none');
        });
        $('.type').trigger('change');
    })
</script> 
@endpush