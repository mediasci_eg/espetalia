<div class="card">
    <div class="card-content">
        <div class="card-body">
          @success
          @errors
            <form action="backend/posts/store" method="post" id="bu-form">
                @csrf
                <input type="hidden" name="id" value="{{$post->id}}">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                          <label for="title">Title</label>
                          <input required placeholder="Post Title" type="text" name="title" value="{{$post->title}}" id="name" class="form-control">
                        </div>
                    </div>
                    
                    <div class="col-12">
                      <div class="form-group">
                          <label class="control-label">Content</label>
                         <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css()?>
                         <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('content', 'content', $post->content)?>
                         <?=App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js()?>
                      </div>
                  </div>
            
                    <!-- <div class="col-6">
                       <div class="form-group">
                         <label for="image">Image</label>
                         {{--{!! Media::selector('image',[$user->image],false) !!} --}}
                         {!! Media::ImageUploader(['name'=>'image','image'=>$post->image])!!}
                       </div>
                    </div> -->
                    
                </div>
                <hr>
                <div class="row">
                    <button type="submit" class="btn btn-xs btn-primary">Save <i class="fas fa-save    "></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('script')
    {{-- <script>
      $(document).ready(function(){
        
        $('#bu-form').validate({
          rules:{
            'name':{required:true},
            'email':{
              'required':true,
              'email':true
            },
             @if ($user->email == '')
            "password": {required: true,minlength:8},
            "password_confirmation": {required: true, equalTo:'#password'}
            @endif
          }
        })
      });
    </script> --}}
@endpush