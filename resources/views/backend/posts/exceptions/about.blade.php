<?php
use App\Http\Controllers\Helpers\Upload;
?>
<div class="card">
    <div class="card-content">
        <div class="card-body">
            @success
            @errors
            <form action="./backend/posts/store" method="post" id="bu-form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$post->id}}">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="page_title">Page Title</label>
                            <input required placeholder="Page Title" type="text" name="title" value="{{$post->title}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="short_title">Short Title</label>
                            <input placeholder="Short Title" type="text" name="data[short_title]" value="{{$post->data['short_title']}}" id="name" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label">Short Content</label>
                            <textarea name="data[short_content]" class="form-control" cols="60" rows="4" placeholder="Please Enter Short Content">{{$post->data['short_content']}}</textarea>
                        </div>
                    </div>



                    <div class="col-5">
                        <div class="form-group">
                            <label class="control-label">Image</label>
                            <input type="file" class='form-control' value="{{$post->image}}" name="image">
                            @if($post->image !='')
                            <img src='{{url('uploads/'.$post->image)}}' style='width:200px'>
                            @endif
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="control-label">Content</label>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css() ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('content', 'content', $post->content) ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js() ?>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <input required placeholder="Who We Are" type="text" name="data[sub_title_1]" value="{{@$post->data['sub_title_1']}}" class="form-control">
<?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('sub_content_1', 'data[sub_content_1]', @$post->data['sub_content_1']) ?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <input required placeholder="Our Vision" type="text" name="data[sub_title_2]" value="{{@$post->data['sub_title_2']}}" class="form-control">
<?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('sub_content_2', 'data[sub_content_2]', @$post->data['sub_content_2']) ?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <input required placeholder="Our Mission" type="text" name="data[sub_title_3]" value="{{@$post->data['sub_title_3']}}" class="form-control">
<?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('sub_content_3', 'data[sub_content_3]', @$post->data['sub_content_3']) ?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <input required placeholder="Our Facilities" type="text" name="data[sub_title_4]" value="{{@$post->data['sub_title_4']}}" class="form-control">
<?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('sub_content_4', 'data[sub_content_4]', @$post->data['sub_content_4']) ?>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <h2>Opening Hours</h2>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="day_1">Day 1</label>
                            <input placeholder="Day" type="text" name="data[day_1]" value="{{$post->data['day_1']}}" id="name" class="form-control mb-1">
                            <input placeholder="Time" type="text" name="data[time_1]" value="{{$post->data['time_1']}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="day_2">Day 2</label>
                            <input placeholder="Day" type="text" name="data[day_2]" value="{{$post->data['day_2']}}" id="name" class="form-control mb-1">
                            <input placeholder="Time" type="text" name="data[time_2]" value="{{$post->data['time_2']}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="day_3">Day 3</label>
                            <input placeholder="Day" type="text" name="data[day_3]" value="{{$post->data['day_3']}}" id="name" class="form-control mb-1">
                            <input placeholder="Time" type="text" name="data[time_3]" value="{{$post->data['time_3']}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="day_4">Day 4</label>
                            <input placeholder="Day" type="text" name="data[day_4]" value="{{$post->data['day_4']}}" id="name" class="form-control mb-1">
                            <input placeholder="Time" type="text" name="data[time_4]" value="{{$post->data['time_4']}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="day_1">Day 5</label>
                            <input placeholder="Day" type="text" name="data[day_5]" value="{{$post->data['day_5']}}" id="name" class="form-control mb-1">
                            <input placeholder="Time" type="text" name="data[time_5]" value="{{$post->data['time_5']}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="day_1">Day 6</label>
                            <input placeholder="Day" type="text" name="data[day_6]" value="{{$post->data['day_6']}}" id="name" class="form-control mb-1">
                            <input placeholder="Time" type="text" name="data[time_6]" value="{{$post->data['time_6']}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="day_7">Day 7</label>
                            <input placeholder="Day" type="text" name="data[day_7]" value="{{$post->data['day_7']}}" id="name" class="form-control mb-1">
                            <input placeholder="Time" type="text" name="data[time_7]" value="{{$post->data['time_7']}}" id="name" class="form-control">
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <h2>Emergencies</h2>
                    </div>
                     <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label">Emergency Content</label>
                            <textarea required name="data[emergency_content]" class="form-control" cols="65" rows="4" placeholder="Please Enter Emergency Content" >{{$post->data['emergency_content']}}</textarea>
                        </div>
                    </div>
                    
                     <div class="col-5">
                        <div class="form-group">
                            <label for="emergency_phone">Emergency Phone</label>
                            <input required placeholder="Emergency Phone" type="text" name="data[emergency_phone]" value="{{$post->data['emergency_phone']}}" id="name" class="form-control">
                        </div>
                    </div>


                    

                </div>
                <hr>
                <div class="row">
                    <button type="submit" class="btn btn-xs btn-primary">Save <i class="fas fa-save    "></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
