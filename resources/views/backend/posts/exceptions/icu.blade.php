<?php

use App\Http\Controllers\Helpers\Upload;
?>
<style>
        .remove_image{
            position: absolute;
            padding: 2px 8px;
            background: #404E67;
            border-radius: 50%;
            color:#fff;
        }
        .remove_image:hover{
            color:#fff;
            transform: scale(1.2);
        }
        #images_area img{
            width:100%;
            height:150px;
            border-radius: 50%;
        }

</style>
<div class="card">
    <div class="card-content">
        <div class="card-body">
            @success
            @errors
            <form action="./backend/posts/store" method="post" id="bu-form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$post->id}}">
                <div class="row">
                        <div class="col-12">
                                <div class="form-group">
                                    <label class="control-label">Main Image</label>
                                    <input type="file" class='form-control' value="{{$post->image}}" name="image">
                                    @if($post->image !='')
                                    <img src='{{url('uploads/'.$post->image)}}' style='width:200px'>
                                    @endif
                                </div>
                            </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="page_title">Page Title</label>
                            <input required placeholder="Page Title" type="text" name="title" value="{{$post->title}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label">Short Description For Home page</label>
                                <textarea name="data[short_content]" class="form-control" cols="60" rows="4" placeholder="Please Enter Short Description">{{@$post->data['short_content']}}</textarea>
                            </div>
                        </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="control-label">Main Content</label>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css() ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('content', 'content', $post->content) ?>
<?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js() ?>
                        </div>
                    </div>
                    <div class="col-12">
                            <div class="form-group">
                                <label class="control-label">Gallery</label>
                                <input type="file" class='form-control' id='gallery' value="" name="" multiple>
                                <span class="error_span" style="display:none;color:red;"></span>
                            </div>
                        </div>
                    </div>
                        <div id="images_area" class="row">
                            <?php if($post->images()->count() > 0){
                                foreach($post->images()->get() as $item){?>
                        <div class="col-2" style="position: relative;">
                            <a href="JavaScript:void(0)" class="remove_image">x</a>
                            <img src="uploads/{{$item->image}}">
                            <input type="hidden" name="images[]" value="{{$item->image}}">
                        </div> 
                        <?php }
                    }?>
                        </div>
                <hr>
                <div class="row">
                    <button type="submit" class="btn btn-xs btn-primary">Save <i class="fas fa-save    "></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#gallery").on("change",function(e){
        e.preventDefault();
        var me = $(this);
        var extentions = ['jpg','png','jpeg','JPG','JPEG','PNG','gif'];
        if (e.target.files.length > 0) {
            var span_errors = '';
            var upload_flag = true;
            for(var image = 0;image< e.target.files.length ; image++){
        var image_name = e.target.files[image].name;
        var image_extention = image_name.replace(/^.*\./, '');
        if(jQuery.inArray(image_extention,extentions) < 0){
            span_errors += image_extention+' files are not supported <br>';
         upload_flag = false;
        }
       if(upload_flag){
        let formData = new FormData();
        formData.append("image", e.target.files[image]);
        var errors="";
        $.ajax({
         type: 'post',
         url: "{{url('backend/upload_image/image')}}",
         data: formData,
         processData: false,
         contentType: false,
         success: function (response) {
             $("#images_area").append('<div class="col-2" style="position: relative;"> <a  href="JavaScript:void(0)" class="remove_image">x</a><img src="uploads/'+response.name+'"><input type="hidden" name="images[]" value="'+response.name+'"></div>');
         },
         statusCode:{
             500:function(){
               me.next('.error_span').text('Internal server error');
               me.next('.error_span').show();
             },
             422:function(response){
                 errors += response.responseJSON.message;
               for(var er_in in response.responseJSON.errors){
                   for(var er_me in response.responseJSON.errors[er_in]){
                       errors += "<br>"+response.responseJSON.errors[er_in][er_me];
                   }
               }
               me.next('.error_span').text(errors);
               me.next('.error_span').show();
             }
         }
       });
    }
    upload_flag = true;
     }
     me.val('');
     if(span_errors != ''){
        me.next('.error_span').html(span_errors);
       me.next('.error_span').show();
     }
     }
    });

    $(document).on("click",'.remove_image',function(){
        $(this).parent().remove();
    })
</script>
