<?php
use App\Http\Controllers\Helpers\Upload;
?>
<div class="card">
    <div class="card-content">
        <div class="card-body">
            @success
            @errors
            <form action="./backend/posts/store" method="post" id="bu-form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$post->id}}">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="page_title">Page Title</label>
                            <input required placeholder="Page Title" type="text" name="title" value="{{$post->title}}" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="control-label">Main Content</label>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css() ?>
                            <?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('content', 'content', $post->content) ?>
<?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js() ?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <input required placeholder="sub title 1" type="text" name="data[sub_title_1]" value="{{@$post->data['sub_title_1']}}" class="form-control">
<?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('sub_content_1', 'data[sub_content_1]', $post->data['sub_content_1']) ?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <input required placeholder="sub title 2" type="text" name="data[sub_title_2]" value="{{@$post->data['sub_title_2']}}" class="form-control">
<?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('sub_content_2', 'data[sub_content_2]', $post->data['sub_content_2']) ?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <input required placeholder="sub title 3" type="text" name="data[sub_title_3]" value="{{@$post->data['sub_title_3']}}" class="form-control">
<?= App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('sub_content_3', 'data[sub_content_3]', $post->data['sub_content_3']) ?>
                        </div>
                    </div>

                </div>
                <hr>
                <div class="row">
                    <button type="submit" class="btn btn-xs btn-primary">Save <i class="fas fa-save"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
