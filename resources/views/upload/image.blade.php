<input type='file'  data-name='{{$name}}' class='form-control images' >
<img src="./assets/images/loading.gif" style="display:none;width:75px;height:75px;" id="upload-loading" />
<ul class='preview-container ' data-hasOne=''>
    @if($value !='')
        <li class='image_container' draggable='true' ondragenter='dragenter(event)' ondragstart='dragstart(event)'>
            <a href='#' class='delete_image'>✖</a>
            <input type='hidden' name='{{$name}}' value='{{$value}}' />
            <img style='width:100px;margin:0 3px' src='{{url('uploads/'.$value )}}'>
        </li>
    @endif
</ul>


@push('footer')
    @include('upload.js')
@endpush

