<script>
        function uploadImage(selector) {
            var file_data = selector.prop("files")[0];
    
            var form_data = new FormData();
            form_data.append("file", file_data);
            $.ajax({
                //headers: {
                  //  'X-CSRF-TOKEN': $('input[name="_token"]').val()
                //},
                type: 'post',
                url: "{{url('backend/upload-image')}}",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                beforeSend: function () {
                    selector.parent().parent().find(".uploading").removeClass('hide');
                    $('#upload-loading').show();
                    $('input[type=submit]').hide();
                },
                complete: function (result) {
                    var content = $.parseJSON(result.responseText);
                    if (content.status == 0)
                    {
                        $('#upload-loading').hide();
                        $('input[type=submit]').show();
                        alert("Error,can't upload the image");
                        return;
                    }
    
                    var element = selector.parent().parent().find('.preview-container');
    
                    if (typeof selector.attr('data-name') != 'undefined')
                        name = selector.attr('data-name');
                    else
                        name = 'images[]';
                    var text = '\n\
                    <li class="image_container" >\n\
                            <a href="#" class="delete_image" >✖</a>\n\
                            <img style="width:100px;margin:0 3px" id=""  src="{{url('uploads')}}/' + content.file + '" />\n\
                            <input type="hidden" name="' + name + '"  value="' + content.file + '" />\n\
                    </li> \n\
                    ';
    
                    if (typeof element.attr('data-hasOne') == 'undefined')
                        element.append(text).show();
                    else
                        element.html(text).show();

                        $('#upload-loading').hide();
                        $('input[type=submit]').show();
                }
            });
        }
        $(document).ready(function () {
            //--------------------------
            $(document).on("change", "input:file.images", function () {
                uploadImage($(this));
            });
            $(document).on('click', '.delete_image', function (e) {
                e.preventDefault();
                var id = $(this).parent().find('img').attr('id');
                if (id > 0)
                {
                    $('form').append("<input type='hidden' name='deleted_images[]' value='" + id + "' >");
                }
                $(this).parent().remove();
            });
        });
            //--------------------------
    </script>

@push('header')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
    ul.preview-container{
        padding: 0;
    }
    li.image_container img {
        width: 100px !important ;
        height: 66px;
        margin: 10px 2px !important;
        border-radius: 10px;
    }
    li.image_container {
        display: inline-block;
        position: relative;
    }
    li.image_container .delete_image {
        position: absolute;
        bottom: -17px;
        left: 48%;
    }
    .sortable .image_container img {
        cursor: grab;
    }
    </style>
@endpush
    

@push('footer')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script>
    $( function() {
        $( ".sortable" ).sortable();
    } );
    </script>
@endpush