@extends('front.layouts.master')
@section('content')

<div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-8" data-bg-img="http://placehold.it/1920x1280">
        <div class="container pt-60 pb-60">
            <!-- Section Content -->
            <div class="section-content">
                <div class="row"> 
                    <div class="col-md-12 xs-text-center">
                        <h3 class="title text-white">Department Details</h3>
                        <ol class="breadcrumb mt-10 white">
                            <li><a class="text-white" href="">Home</a></li>
                            <!--<li><a class="text-white" href="#">Pages</a></li>-->
                            <li class="active text-theme-colored">Department Details</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Section: Departments -->
    <section>
        <div class="container">
            <div class="row mtli-row-clearfix">
                <div class="col-sm-6 col-md-12 col-lg-12">
                    <div class="campaign maxwidth500 mb-sm-30">
                        <div class="thumb">
                            <img src="{{url('uploads/'.$department->image)}}" alt="" class="img-fullwidth">
                            <div class="campaign-overlay"></div>
                        </div>
                        <h4 class=""><a href="javascript:void(0)">{{$department->name}}</a></h4>
                    </div>
                    {!!$department->content!!}
                </div>
            </div>
            <!--            <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4 pb-sm-20">
                                <h3 class="title mb-30 line-bottom">Treatments</h3>
                                <ul class="list-border-bottom no-padding">
                                    <li>
                                        <h5>Colonoscopy <span class="pull-right flip font-weight-400 pr-20">$220</span></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis quas ullam perferendis facilis <a class="text-theme-colored font-13" href="#"> read more</a></p>
                                    </li>
                                    <li>
                                        <h5>Colonoscopy <span class="pull-right flip font-weight-400 pr-20">$220</span></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis quas ullam perferendis facilis <a class="text-theme-colored font-13" href="#"> read more</a></p>
                                    </li>
                                    <li>
                                        <h5>Colonoscopy <span class="pull-right flip font-weight-400 pr-20">$220</span></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis quas ullam perferendis facilis <a class="text-theme-colored font-13" href="#"> read more</a></p>
                                    </li>
                                </ul>
                                <a class="text-theme-colored font-weight-600 font-13" href="#">See All List →</a>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 pb-sm-20">
                                <h3 class="title mb-30 line-bottom">Laboratory Tests</h3>
                                <ul class="list-border-bottom no-padding">
                                    <li>
                                        <h5>Colonoscopy <span class="pull-right flip font-weight-400 pr-20">$220</span></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis quas ullam perferendis facilis <a class="text-theme-colored font-13" href="#"> read more</a></p>
                                    </li>
                                    <li>
                                        <h5>Colonoscopy <span class="pull-right flip font-weight-400 pr-20">$220</span></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis quas ullam perferendis facilis <a class="text-theme-colored font-13" href="#"> read more</a></p>
                                    </li>
                                    <li>
                                        <h5>Colonoscopy <span class="pull-right flip font-weight-400 pr-20">$220</span></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis quas ullam perferendis facilis <a class="text-theme-colored font-13" href="#"> read more</a></p>
                                    </li>
                                </ul>
                                <a class="text-theme-colored font-weight-600 font-13" href="#">See All List →</a>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 pb-sm-20">
                                <h3 class="title mb-30 line-bottom">Investigations</h3>
                                <ul class="list-border-bottom no-padding">
                                    <li>
                                        <h5>Colonoscopy <span class="pull-right flip font-weight-400 pr-20">$220</span></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis quas ullam perferendis facilis <a class="text-theme-colored font-13" href="#"> read more</a></p>
                                    </li>
                                    <li>
                                        <h5>Colonoscopy <span class="pull-right flip font-weight-400 pr-20">$220</span></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis quas ullam perferendis facilis <a class="text-theme-colored font-13" href="#"> read more</a></p>
                                    </li>
                                    <li>
                                        <h5>Colonoscopy <span class="pull-right flip font-weight-400 pr-20">$220</span></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis quas ullam perferendis facilis <a class="text-theme-colored font-13" href="#"> read more</a></p>
                                    </li>
                                </ul>
                                <a class="text-theme-colored font-weight-600 font-13" href="#">See All List →</a>
                            </div>
                        </div>-->
        </div>
    </section>

    <!-- Section: Doctors -->
    <section id="doctors">
        <div class="container">
            <div class="section-title text-center">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="text-uppercase mt-0 line-height-1">Our Doctors</h2>
                        <div class="title-icon">
                            <img class="mb-10" src="images/title-icon.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mtli-row-clearfix">
                <div class="col-md-12">
                    <div class="owl-carousel-4col">
                        @foreach($doctors as $doctor)
                        <div class="item">
                            <div class="team-members border-bottom-theme-color-2px text-center maxwidth400">
                                <div class="team-thumb">
                                    <img class="img-fullwidth" alt="" src="{{url('uploads/'.$doctor->image)}}">
                                    <div class="team-overlay"></div>
                                </div>
                                <div class="team-details bg-silver-light pt-10 pb-10">
                                    <h4 class="text-uppercase font-weight-600 m-5">{{$doctor->name}}</h4>
                                    <h6 class="text-theme-colored font-15 font-weight-400 mt-0">{{$doctor->department->name}}</h6>                    
                                    <a class="btn btn-theme-colored btn-sm flip" href="{{url('/doctors/'.$doctor->id)}}">view details</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@stop