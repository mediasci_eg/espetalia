@extends('front.layouts.master')
@section('content')

<div class="news-events-detail">
    <?php

    use \App\Http\Controllers\Helpers\Youtube;

$title = ' News & Events.';
    $image = 'front/images/news-event/intro-student.png';
    ?>
    @include ('front.layouts.top_header')

    <div class="latest-news g-padding">
        <div class="container">
            <h2>{{$item->title}}</h2>
            <p>{!!$item->content!!}</p>

            <div class="our-partner">
                <div class="our-university">
                    <div class="slider">
                        <?php
                        $images = [];
                        foreach ($item->images as $image) {
                            $images[] = 'uploads/' . $image;
                        }
                        ?>
                        @include ('front.layouts.owl-slider-campus')
                    </div>
                </div>
            </div>
            <!--video-->
            @if(isset($item->video))
            <div class="watch">
                <h3>Video</h3>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe width="560" height="315" src="{{Youtube::streamLink($item->video)}}" frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                </div>
            </div>
            @endif

            <p class="time">{{date('d M Y',strtotime($item->date))}}</p>
        </div>
    </div>
</div>

@stop