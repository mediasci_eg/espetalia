@extends('front.layouts.master')
@section('content')

<div class="main-content">
    <?php

    use \App\Http\Controllers\Helpers\Youtube;
    ?>
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-8" data-bg-img="http://placehold.it/1920x1280">
        <div class="container pt-60 pb-60">
            <!-- Section Content -->
            <div class="section-content">
                <div class="row"> 
                    <div class="col-md-12 xs-text-center">
                        <h3 class="title text-white">{{$item->title}}</h3>
                        <ol class="breadcrumb mt-10 white">
                            <li><a class="text-white" href="">Home</a></li>
                            <li><a class="text-white" href="{{url('news-events')}}">News</a></li>
                            <li class="active text-theme-colored">{{$item->title}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section>
        <div class="container">
            <div class="row multi-row-clearfix">
                <div class="blog-posts">
                    <div class="col-sm-12 col-md-12">
                        <article class="post clearfix mb-30 bg-lighter">
                             <div class="entry-header">
                                @if(isset($item->image))
                                <div class="post-thumb thumb"> 
                                    <img src="{{url('uploads/'.$item->image)}}" alt="" class="img-responsive img-fullwidth"> 
                                </div>
                                @elseif(isset($item->link))
                                <div class="watch">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe width="360" height="240" src="{{Youtube::streamLink($item->link)}}" frameborder="0"
                                                allowfullscreen></iframe>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="entry-content p-20 pr-10">
                                <div class="entry-meta media mt-0 no-bg no-border">
                                    <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                                        <ul>
                                            <li class="font-16 text-white font-weight-600">{{Carbon\Carbon::parse($item->date)->format('d')}}</li>
                                            <li class="font-12 text-white text-uppercase">{{Carbon\Carbon::parse($item->date)->format('M')}}</li>
                                        </ul>
                                    </div>
                                    <div class="media-body pl-15">
                                        <div class="event-content pull-left flip">
                                            <h4 class="entry-title text-white text-uppercase m-0 mt-5"><a>{{$item->title}}</a></h4>                      
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-10">{!!$item->content!!}</p>
                                <div class="clearfix"></div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@stop