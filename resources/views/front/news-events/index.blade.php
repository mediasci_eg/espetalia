@extends('front.layouts.master')
@section('content')

<div class="main-content">
 <?php

    use \App\Http\Controllers\Helpers\Youtube;
    ?>
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-8" data-bg-img="http://placehold.it/1920x1280">
        <div class="container pt-60 pb-60">
            <!-- Section Content -->
            <div class="section-content">
                <div class="row"> 
                    <div class="col-md-12 xs-text-center">
                        <h3 class="title text-white">News</h3>
                        <ol class="breadcrumb mt-10 white">
                            <li><a class="text-white" href="">Home</a></li>
                            <li class="active text-theme-colored">News</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section>
        <div class="container">
            <div class="row multi-row-clearfix">
                <div class="blog-posts">
                    @foreach($news_events as $new)
                    <div class="col-sm-6 col-md-4">
                        <article class="post clearfix mb-30 bg-lighter">
                            <div class="entry-header">
                                @if(isset($new->image))
                                <div class="post-thumb thumb"> 
                                    <img src="{{url('uploads/'.$new->image)}}" alt="" class="img-responsive img-fullwidth"> 
                                </div>
                                @elseif(isset($new->link))
                                <div class="watch">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe width="560" height="315" src="{{Youtube::streamLink($new->link)}}" frameborder="0"
                                                allowfullscreen></iframe>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="entry-content p-20 pr-10">
                                <div class="entry-meta media mt-0 no-bg no-border">
                                    <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                                        <ul>
                                            <li class="font-16 text-white font-weight-600">{{Carbon\Carbon::parse($new->date)->format('d')}}</li>
                                            <li class="font-12 text-white text-uppercase">{{Carbon\Carbon::parse($new->date)->format('M')}}</li>
                                        </ul>
                                    </div>
                                    <div class="media-body pl-15">
                                        <div class="event-content pull-left flip">
                                            <h4 class="entry-title text-white text-uppercase m-0 mt-5"><a href="{{url('news-events-detail/'.$new->id)}}">{{$new->title}}</a></h4>                      
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-10">{{str_limit($new->content,'140')}}</p>
                                <a href="{{url('news-events-detail/'.$new->id)}}" class="btn-read-more">Read more</a>
                                <div class="clearfix"></div>
                            </div>
                        </article>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
</div>

@stop