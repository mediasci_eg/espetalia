@extends('front.layouts.master')
@section('content')

<!-- Start main-content -->
<div class="main-content">
    <!-- Section: home -->
    <section id="home" class="divider">
        <div class="container-fluid p-0">

            <!-- Slider Revolution Start -->
            <div class="rev_slider_wrapper">
                <div class="rev_slider" data-version="5.0">
                    <ul>
                        @foreach($sliders as $slider)
                        <li data-index="rs-{{$slider->id}}" data-transition="random" data-slotamount="7"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="{{url('uploads/'.$slider->image)}}"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="{{url('uploads/'.$slider->image)}}"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>


                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption tp-resizeme text-uppercase text-white bg-theme-colored-transparent pl-40 pr-40"
                                 id="rs-{{$slider->id}}-layer-2"

                                 data-x="['center']"
                                 data-hoffset="['0']"
                                 data-y="['middle']"
                                 data-voffset="['-20']"
                                 data-fontsize="['48']"
                                 data-lineheight="['70']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-transform_idle="o:1;s:500"
                                 data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                 data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                 data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                 data-start="1000" 
                                 data-splitin="none" 
                                 data-splitout="none" 
                                 data-responsive_offset="on"
                                 style="z-index: 7; white-space: nowrap; font-weight:600; border-radius:45px;">{{$slider->name}}
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption tp-resizeme text-center text-black" 
                                 id="rs-{{$slider->id}}-layer-3"

                                 data-x="['center']"
                                 data-hoffset="['0']"
                                 data-y="['middle']"
                                 data-voffset="['50','60','70']"
                                 data-fontsize="['16','18','24']"
                                 data-lineheight="['28']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-transform_idle="o:1;s:500"
                                 data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                 data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                 data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                 data-start="1400" 
                                 data-splitin="none" 
                                 data-splitout="none" 
                                 data-responsive_offset="on"
                                 style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">{!!$slider->content!!}
                            </div>

                            <!-- LAYER NR. 4 -->
                            @if($slider->link != '')
                            <div class="tp-caption tp-resizeme" 
                                 id="rs-{{$slider->id}}-layer-4"

                                 data-x="['center']"
                                 data-hoffset="['0']"
                                 data-y="['middle']"
                                 data-voffset="['135','145','155']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-transform_idle="o:1;"
                                 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                                 data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                 data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                                 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                 data-start="1400" 
                                 data-splitin="none" 
                                 data-splitout="none" 
                                 data-responsive_offset="on"
                                 style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-lg btn-theme-colored pl-20 pr-20" href="{{$slider->link}}">View Details</a> 
                            </div>
                            @endif
                        </li>
                        @endforeach
                    </ul>
                </div><!-- end .rev_slider -->
            </div>
            <!-- end .rev_slider_wrapper -->
            <script>
                $(document).ready(function (e) {
                    var revapi = $(".rev_slider").revolution({
                        sliderType: "standard",
                        sliderLayout: "auto",
                        dottedOverlay: "none",
                        delay: 5000,
                        navigation: {
                            keyboardNavigation: "off",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation: "off",
                            onHoverStop: "off",
                            touch: {
                                touchenabled: "on",
                                swipe_threshold: 75,
                                swipe_min_touches: 1,
                                swipe_direction: "horizontal",
                                drag_block_vertical: false
                            },
                            arrows: {
                                style: "zeus",
                                enable: true,
                                hide_onmobile: true,
                                hide_under: 600,
                                hide_onleave: true,
                                hide_delay: 200,
                                hide_delay_mobile: 1200,
                                tmp: '<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div> </div>',
                                left: {
                                    h_align: "left",
                                    v_align: "center",
                                    h_offset: 30,
                                    v_offset: 0
                                },
                                right: {
                                    h_align: "right",
                                    v_align: "center",
                                    h_offset: 30,
                                    v_offset: 0
                                }
                            },
                            bullets: {
                                enable: true,
                                hide_onmobile: true,
                                hide_under: 600,
                                style: "hebe",
                                hide_onleave: false,
                                direction: "horizontal",
                                h_align: "center",
                                v_align: "bottom",
                                h_offset: 0,
                                v_offset: 30,
                                space: 5,
                                tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title"></span>'
                            }
                        },
                        responsiveLevels: [1240, 1024, 778],
                        visibilityLevels: [1240, 1024, 778],
                        gridwidth: [1170, 1024, 778, 480],
                        gridheight: [680, 500, 400, 400],
                        lazyType: "none",
                        parallax: {
                            origo: "slidercenter",
                            speed: 1000,
                            levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
                            type: "scroll"
                        },
                        shadow: 0,
                        spinner: "off",
                        stopLoop: "on",
                        stopAfterLoops: 0,
                        stopAtSlide: -1,
                        shuffle: "off",
                        autoHeight: "off",
                        fullScreenAutoWidth: "off",
                        fullScreenAlignForce: "off",
                        fullScreenOffsetContainer: "",
                        fullScreenOffset: "0",
                        hideThumbsOnMobile: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                        }
                    });
                });
            </script>
            <!-- Slider Revolution Ends -->

        </div>
    </section>

    <!-- Section: home-boxes -->
    <section>
        <div class="container pb-0">
            <div class="section-content">
                <div class="row equal-height-inner">
                    <div class="col-sm-12 col-md-4 pr-0 pr-sm-15 sm-height-auto mt-sm-0" data-margin-top="-150px">
                        <div class="sm-height-auto bg-theme-colored-darker2">
                            <div class="p-30">
                                <h3 class="text-uppercase text-white mt-0">{{@$post->data['sub_title_1']}}</h3>
                                <p class="text-white">{{str_limit(@$post->data['sub_content_1'],150)}}</p>
                                <a href="{{url('/about')}}" class="btn-read-more text-white">Read more</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 pl-0 pl-sm-15 pr-0 pr-sm-15 sm-height-auto mt-sm-0" data-margin-top="-150px">
                        <div class="sm-height-auto bg-theme-colored">
                            <div class="p-30">
                                <!--<h3 class="text-white text-uppercase mt-0">Opening Hours</h3>-->
                                <!--                                <div class="opening-hours">
                                                                    <ul class="list-unstyled text-white">
                                                                        @for($i = 1 ; $i<=7 ; $i++)
                                                                        @if($post->data['day_'.$i])
                                                                        <li class="clearfix"> <span>{{$post->data['day_'.$i]}}</span>
                                                                            <div class="value">{{$post->data['time_'.$i]}}</div>
                                                                        </li>
                                                                        @endif
                                                                        @endfor
                                                                    </ul>
                                                                </div>-->
                                <a href="#contact" class="btn btn-read-more text-gray-lighter mt-10"><h3 class="text-white text-uppercase mt-0">Book an appointment</h3></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 pl-0 pl-sm-15 sm-height-auto mt-sm-0" data-margin-top="-150px">
                        <div class="sm-height-auto bg-theme-colored-darker2">
                            <div class="p-30">
                                <h3 class="text-uppercase text-white mt-0">Stroke Emergency</h3>
                                <p class="text-white">{{str_limit($post->data['emergency_content'],150)}}</p>
                                <h3 class="text-white">{{$post->data['emergency_phone']}}</h3>
                                <a href="{{url('/about')}}" class="btn-read-more text-white">Read more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Section: Departments -->
    <section class="bg-lighter">
        <div class="container">
            <div class="section-title text-center">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="text-uppercase mt-0 line-height-1">Our Services</h2>
                        <div class="title-icon">
                            <img class="mb-10" src="front/images/title-icon.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-content">
                <div class="row">
                    @foreach($services as $service)
                    <div class="col-xs-12 col-sm-6 col-md-3 mb-30">
                        <img class="img-fullwidth" alt="" src="{{url('uploads/'.$service->image)}}">
                        <h4>{{$service->title}}</h4>
                        <p>{{str_limit($service->short_content,100)}}</p>
                        <a href="{{url($service->slug)}}" class="btn-read-more text-theme-colored">Read more</a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <!-- Section: Doctors -->
    <section id="doctors">
        <div class="container">
            <div class="section-title text-center">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="text-uppercase mt-0 line-height-1">Our Doctors</h2>
                        <div class="title-icon">
                            <img class="mb-10" src="front/images/title-icon.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mtli-row-clearfix">
                <div class="col-md-12">
                    <div class="owl-carousel-4col">
                        @foreach($doctors as $doctor)
                        <div class="item">
                            <div class="team-members border-bottom-theme-color-2px text-center maxwidth400">
                                <div class="team-thumb">
                                    <img class="img-fullwidth" alt="" src="{{url('uploads/'.$doctor->image)}}">
                                    <div class="team-overlay"></div>
                                </div>
                                <div class="team-details bg-silver-light pt-10 pb-10 " style="position: initial; height: 25vh; display: flex; flex-wrap: wrap;  justify-content: center;   align-items: flex-end;">
                                    <h4 class="text-uppercase font-weight-600 m-5" style="display: block;  width: 100%;">
   {{$doctor->name}}</h4> 
                                    <h6 class="text-theme-colored font-15 font-weight-400 mt-0" style="display: block;  width: 100%;">{{$doctor->title}}</h6>                    
                                    <a class="btn btn-theme-colored btn-sm flip" href="{{url('/doctors/'.$doctor->id)}}">view details</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- Section: testmonials -->
<!--    <section class="divider parallax layer-overlay overlay-white-9" data-bg-img="http://placehold.it/1920x1280" data-parallax-ratio="0.7">>
        <div class="container pb-40">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="title line-height-1 line-bottom mt-0 mt-sm-30 mb-30"><i class="fa fa-thumb-tack text-theme-colored mr-10"></i>Clients Say</h3>
                        <div class="testimonial style1 owl-carousel-1col">
                            {{--  @foreach($testimonials as $testimonial)  --}}
                            <div class="item">
                                <div class="comment border-radius-15px">
                                    {{--  <p>{{$testimonial->content}}</p>  --}}
                                </div>
                                <div class="content mt-20">
                                    <div class="thumb pull-right">
                                        <img class="img-circle" alt="" src="http://placehold.it/75x75">
                                    </div>
                                    <div class="patient-details text-right pull-right mr-20 mt-10">
                                        {{--  <h5 class="">{{$testimonial->name}}</h5>  --}}
                                    </div>
                                </div>
                            </div>
                            {{--  @endforeach  --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>-->

    <!-- Section: Contact -->
    <section id="contact" class="bg-lighter">
        <div class="container pb-0">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-4" data-wow-duration="1.4s">
                        <img src="{{url('uploads/'.$post->image)}}" alt="">
                    </div>
                    <div class="col-md-8 wow fadeInRight mt-10" data-wow-duration="1.4s">
                        <h3 class="title line-bottom line-height-1 mt-0 mb-30">Book an <span class="text-theme-colored">Appointment!</span></h3>          
                        Contact Form 
                        <form id="contact_form_home" name="contact_form_home" class="" action="make-appointment" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group mb-20">
                                        <input name="full_name" class="form-control" type="text" placeholder="Enter Full Name" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group mb-20">
                                        <input name="phone" class="form-control phone" required pattern=".{11,11}" title="it must be 11 number"  onkeypress="return isNumber(event)" type="text" placeholder="Enter Phone">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group mb-30">
                                        <div class="styled-select">
                                            <select id="car_select" name="department_id" class="form-control" required>
                                                <option value="">- Select Your Department -</option>
                                                @foreach($departments as $department)
                                                <option value="{{$department->id}}">{{$department->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group mb-30">
                                        <input name="date" class="form-control" type="date" placeholder="Reservation Date" aria-required="true" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input name="form_botcheck" class="form-control" type="hidden" value=""/>
                                <button type="submit" class="btn btn-flat btn-theme-colored text-uppercase mt-0 mb-sm-30">Submit Reservation</button>
                                <button type="reset" class="btn btn-flat btn-theme-colored text-uppercase mt-0 mb-sm-30">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- end main-content -->

<script>
    $(document).ready(function () {
        $("#contact_form_home").on('submit', function (e) {
            var berrors = '';
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "<?= url('/make-appointment') ?>",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                data: $("#contact_form_home").serialize(),
                success: function (response) {
                    if (JSON.parse(response).status == 'success')
                        alert('Appointment Submitted successfully');
                    document.getElementById("contact_form_home").reset();
                },
                statusCode: {
                    500: function (response) {
                        alert(response.responseJSON.message);
                    },
                    422: function (response) {
                        berrors += response.responseJSON.message;
                        for (var er_in in response.responseJSON.errors) {
                            for (var er_me in response.responseJSON.errors[er_in]) {
                                berrors += "<br>" + response.responseJSON.errors[er_in][er_me];
                            }
                        }
                        alert(berrors);
                    },
                    419: function (response) {
                        alert("Sorry, session has expired. Login and try again");
                    }
                    ,
                    404: function (response) {
                        alert("Sorry, the page you are trying to reach is not found");
                    }
                }
            });
        });
    });
</script>
@stop