@extends('front.layouts.master')
@section('content')

<!-- Start main-content -->
<div class="main-content">

  <!-- Section: inner-header -->
  <section class="inner-header divider parallax layer-overlay overlay-dark-8" data-bg-img="http://placehold.it/1920x1280">
    <div class="container pt-60 pb-60">
      <!-- Section Content -->
      <div class="section-content">
        <div class="row"> 
          <div class="col-md-12 xs-text-center">
            <h3 class="title text-white">{{$service_post->title}} Details</h3>
            <ol class="breadcrumb mt-10 white">
              <li><a class="text-white" href="">Home</a></li>
              <li class="active text-theme-colored">{{$service_post->title}}</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Section: Services -->
  <section>
    <div class="container">
      <div class="row mtli-row-clearfix">
        <div class="col-12">
          <div class="event-details mt-30">
                {!!$service_post->content!!}
        </div>
        <?php if($service_post->data != null){
            for($v = 1;$v<=50;$v++){
            if(isset($service_post->data["sub_title_".$v])){
            ?>

          <div class="campaign maxwidth500 mb-sm-30">
            <h4 class="text-theme-colored">{{$service_post->data["sub_title_".$v]}}</h4>
          </div>
          <div class="event-details">
                {!!$service_post->data["sub_content_".$v]!!}
          </div>
        <?php 
    }
            }
        }
        ?>
        </div>
      </div>
    </div>
  </section>
</div>
<!-- end main-content -->

@stop

@push('footer')
<script type="text/javascript">

    $('.modal').on('hidden.bs.modal', function () {

        $('.my_iframe').each(function () {

            var el_src = $(this).attr("src");
            $(this).attr("src", el_src);
        });
    });
</script>

@endpush