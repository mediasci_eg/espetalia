@extends('front.layouts.master')
@section('content')

<div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-8" data-bg-img="http://placehold.it/1920x1280">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-md-12 xs-text-center">
              <h3 class="title text-white">About Us</h3>
              <ol class="breadcrumb mt-10 white">
                <li><a class="text-white" href="">Home</a></li>
                <!--<li><a class="text-white" href="#">Pages</a></li>-->
                <li class="active text-theme-colored">About Us</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Section: about -->
    <section class="">
        <div class="container pb-0">
            <div class="row">
                <div class="col-md-4">
                        <h2 class="text-theme-colored mt-0">{{$post->data['short_title']}}</h2>
                        <p class="font-weight-600">{!!$post->content!!}</p>
                        <?php if($post->data != null){ 
                            for($v = 1;$v<=50;$v++){
                            if(isset($post->data["sub_title_".$v])){
                            ?>
                    <h2 class="text-theme-colored mt-0"> {{$post->data["sub_title_".$v]}}</h2>
                    <p class="font-weight-600">{!!$post->data["sub_content_".$v]!!}</p>
                    <?php }} }?>

                </div>
                <div class="col-md-4">
                    <img src="{{url('uploads/'.$post->image)}}" alt="">
                </div>
                <div class="col-md-4">
                    <div class="border-10px p-30">
                        <h5><i class="fa fa-clock-o text-theme-colored"></i> Opening Hours</h5>
                        <div class="opening-hours text-left">
                            <ul class="list-unstyled">
                                @for($i=1 ; $i<=7 ; $i++)
                                @if($post->data['day_'.$i])
                                <li class="clearfix line-height-1"> <span>{{$post->data['day_'.$i]}}</span>
                                    <div class="value">{{$post->data['time_'.$i]}}</div>
                                </li>
                                @endif
                                @endfor
                            </ul>
                        </div>
                        <h5 class="mt-30"><i class="fa fa-pencil-square-o text-theme-colored"></i>Stroke Emergency</h5>
                        <p class="mt-0">{{$post->data['emergency_content']}}</p>
                        <p class="mt-0">Phone: {{$post->data['emergency_phone']}}</p>
                        <!--<a href="#" class="btn btn-dark btn-theme-colored mt-15">View Hospital Details</a>-->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@stop

@push('footer')
<script type="text/javascript">

    $('.modal').on('hidden.bs.modal', function () {

        $('.my_iframe').each(function () {

            var el_src = $(this).attr("src");
            $(this).attr("src", el_src);
        });
    });
</script>

@endpush