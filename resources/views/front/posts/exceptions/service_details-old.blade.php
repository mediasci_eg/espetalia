<div class="main-content">
        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-8" data-bg-img="http://placehold.it/1920x1280">
            <div class="container pt-60 pb-60">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row"> 
                        <div class="col-md-12 xs-text-center">
                            <h3 class="title text-white">{{$service_post->title}}</h3>
                            <ol class="breadcrumb mt-10 white">
                                <li><a class="text-white" href="">Home</a></li>
                                <!--<li><a class="text-white" href="#">Pages</a></li>-->
                                <li class="active text-theme-colored">{{$service_post->title}}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Section: about -->
        <section class="">
            <div class="container pb-0">
                <div class="row">
                    <div class="col-md-12">
                        <!--<h3 class="text-gray mt-0 mt-sm-30 mb-0">Welcome To</h3>-->
                        <h2 class="text-theme-colored mt-0">{{$service_post->title}}</h2>
                        <p class="font-weight-600">{!!$service_post->content!!}</p>
                    </div>
                    <?php if($service_post->data != null){?>
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php for($v = 1;$v<=50;$v++){
                                if(isset($service_post->data["sub_title_".$v])){
                                ?>
                            <div class="panel panel-default">
                              <div class="panel-heading" role="tab" id="heading{{$v}}">
                                <h4 class="panel-title">
                                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$v}}" aria-expanded="false" aria-controls="collapse{{$v}}">
                                    {{$service_post->data["sub_title_".$v]}}
                                  </a>
                                </h4>
                              </div>
                              <div id="collapse{{$v}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$v}}">
                                <div class="panel-body">
                                    {!!$service_post->data["sub_content_".$v]!!}
                                </div>
                              </div>
                            </div>
                            <?php }}?>
                          </div>
                    </div>
                        <?php }?>
                </div>
            </div>
        </section>
    </div>