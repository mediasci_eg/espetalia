@extends('front.layouts.master')
@section('content')

<div class="{{$post->class}} homepage ">
    <?php
        $title = $post->title;
        $image = $post->image;
    ?>
    @include ('front.layouts.top_header')
    {!!$post->content!!}
</div>

@stop