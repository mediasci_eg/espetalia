@extends('front.layouts.master')
@section('content')

<div class="includedContent"></div>
<script>var AccountsGuid = "1516e953-39d7-4c4b-b441-44f076ed484b"; var lang = "ar"; var sitekey="6Lft3uMUAAAAAKBK8FLuJy9LoU-ZCPLoI2-vyb4M"; $(function () {$(".includedContent").load("https://login.edoctor.clinic/Apps/OnlineReservationv2.html"); });</script>
{{-- <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-8" data-bg-img="http://placehold.it/1920x1280">
        <div class="container pt-60 pb-60">
            <!-- Section Content -->
            <div class="section-content">
                <div class="row"> 
                    <div class="col-md-12 xs-text-center">
                        <h3 class="title text-white">Clinics Schedule</h3>
                        <ol class="breadcrumb mt-10 white">
                            <li><a class="text-white" href="">Home</a></li>
                            <li class="active text-theme-colored">Clinics Schedule</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Section: Departments -->
    <section data-bg-img="./front/images/pattern/p4.png">
        <div class="container" id="show-schedule">
            <div class="section-title text-center">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="text-uppercase mt-0 line-height-1">Our Clinics Schedule</h2>
                        <div class="title-icon">
                            <img class="mb-10" src="./front/images/title-icon.png" alt="">
                        </div>
                        <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>-->
                    </div>
                </div>
            </div>
            <!--        <div class="section-centent">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="services-tab border-10px bg-white">
                            <ul class="nav nav-tabs">
                              <li style="width: 16.5%;" class="active"><a style="padding: 20px;color:#000;" href="#tab11" data-toggle="tab"><span>Saturday</span></a></li>
                              <li style="width: 16.5%;"><a style="padding: 20px;color:#000;"  href="#tab12" data-toggle="tab"><span>Sunday</span></a></li>
                              <li style="width: 16.5%;"><a style="padding: 20px;color:#000;" href="#tab13" data-toggle="tab"><span>Monday</span></a></li>
                              <li style="width: 16.5%;"><a style="padding: 20px;color:#000;" href="#tab14" data-toggle="tab"><span>Tuesday</span></a></li>
                              <li style="width: 16.5%;"><a style="padding: 20px;color:#000;" href="#tab15" data-toggle="tab"><span>Wednesday</span></a></li>
                              <li style="width: 17.5%;"><a style="padding: 20px;color:#000;" href="#tab16" data-toggle="tab"><span>Thursday</span></a></li>
                            </ul>
                            <div class="tab-content">
                              <div class="tab-pane fade in active" id="tab11">
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="thumb">
                                      <img class="img-fullwidth" src="uploads/Saturday.PNG" alt="">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="tab-pane fade" id="tab12">
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="thumb">
                                      <img class="img-fullwidth" src="uploads/Sunday.PNG" alt="">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="tab-pane fade" id="tab13">
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="thumb">
                                      <img class="img-fullwidth" src="uploads/Monday.PNG" alt="">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="tab-pane fade" id="tab14">
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="thumb">
                                      <img class="img-fullwidth" src="uploads/Tuesday.PNG" alt="">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="tab-pane fade" id="tab15">
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="thumb">
                                      <img class="img-fullwidth" src="uploads/Wednesday.PNG" alt="">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="tab-pane fade" id="tab16">
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="thumb">
                                      <img class="img-fullwidth" src="uploads/Thursday.PNG" alt="">
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>-->


            <div class="section-centent">
                <div class="row">
                    <div class="col-md-7">
                        <select name="days" class="option-day type form-control">
                            <option value="" selected disabled>Choose a Day</option>
                            <option value="Saturday">Saturday</option>
                            <option value="Sunday">Sunday</option>
                            <option value="Monday">Monday</option>
                            <option value="Tuesday">Tuesday</option>
                            <option value="Wednesday">Wednesday</option>
                            <option value="Thursday">Thursday</option>
                        </select>
                    </div>

                </div>
            </div>

            <div class='image_container' style="margin-top: 15px;">
                <img src="uploads/{{date('l')}}.PNG" >
            </div>

        </div>
    </section>
</div> --}}
<!-- end main-content -->

@push('footer')
<script>
    $(document).ready(function () {
        $('.option-day').change(function () {
            var selectedDay = $(this).val();
            if (selectedDay == '')
                return;
            var img = '<img src="uploads/' + selectedDay + '.PNG" >';
            $('.image_container').html(img);
        });
    });
</script> 
@endpush
@stop
