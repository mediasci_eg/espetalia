<footer id="footer" class="footer pb-0" data-bg-img="front/images/footer-bg.png" data-bg-color="#25272e">
    <div class="container pb-0" style="padding-top:45px !important">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="widget dark">
                    <h5 class="widget-title line-bottom-theme-colored-2">Quick Contact</h5>

                    <form id="footer_quick_contact_form" name="footer_quick_contact_form" class="quick-contact-form" action="contact-us" method="post">
                        @csrf
                        <div class="form-group col-md-6">
                            <input name="name" class="form-control" type="text" required placeholder="Enter Name">
                        </div>
                        <div class="form-group col-md-6">
                            <input name="phone" class="form-control" type="text" required  pattern=".{11,11}" title="it must be 11 number"  onkeypress="return isNumber(event)" placeholder="Enter Phone">
                        </div>
                        <div class="form-group col-md-6">
                            <input name="mail" class="form-control" type="email"  placeholder="Enter Email" required>
                        </div>
                        <div class="form-group col-md-6">
                            <input name="subject" class="form-control" type="text"  placeholder="Enter Subject" required>
                        </div>
                        <div class="form-group col-md-12">
                            <textarea name="message" class="form-control"  placeholder="Enter Message" rows="3" required></textarea>
                        </div>
                        <div class="form-group col-md-5">
                            <!--<input name="form_botcheck" class="form-control" type="hidden" value="" />-->
                            <button type="submit" class="btn btn-default btn-transparent btn-xs btn-flat mt-0">Send Message</button>
                        </div>
                    </form>

                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="widget dark">
                    <!--<h5 class="widget-title line-bottom-theme-colored-2">Opening Hours</h5>
                    <div class="opening-hours">
                        <ul class="list list-border">
                            @for($i = 1 ; $i<=7 ; $i++)
                            @if($post->data['day_'.$i])
                            <li class="clearfix"> <span><i class="fa fa-clock-o mr-5"></i>{{$post->data['day_'.$i]}}</span>
                                <div class="value pull-right">{{$post->data['time_'.$i]}}</div>
                            </li>
                            @endif
                            @endfor
                        </ul>
                    </div>-->

                    <!-- Google Map HTML Codes -->
                    <div 
                      id="map-canvas-multipointer"
                      data-mapstyle="style1"
                      data-height="300"
                      data-zoom="12"
                      data-marker="front/images/map-marker.png">
                    </div>
                    <!-- Google Map Javascript Codes -->
                   
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="horizontal-contact-widget mt-30 pt-30 text-center">
                    <div class="col-sm-12 col-sm-4">
                        <div class="each-widget"> <i class="pe-7s-phone font-36 mb-10"></i>
                            <h5 class="text-white">Call Us</h5>
                            <p>Phone: <a href="javascript:void(0)">{{$settings['call_us']}}</a></p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-sm-4 mt-sm-50">
                        <div class="each-widget"> <i class="pe-7s-map font-36 mb-10"></i>
                            <h5 class="text-white">Address</h5>
                            <p>{{$settings['address']}}</p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-sm-4 mt-sm-50">
                        <div class="each-widget"> <i class="pe-7s-mail font-36 mb-10"></i>
                            <h5 class="text-white">Email</h5>
                            <p><a href="mailto:{{$settings['email']}}">{{$settings['email']}}</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="list-inline styled-icons icon-hover-theme-colored icon-gray icon-circled text-center mt-30 mb-10">
                    <li><a href="{{$settings['facebook_link']}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="{{$settings['instagram_link']}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid bg-theme-colored p-20">
        <div class="row text-center">
            <div class="col-md-12">
                <p class="text-white font-11 m-0">Copyright &copy;<?= date("Y") ?> <a href="https://media-sci.com/" style="color:#fff">
                    Mediasci
                    <img src="front/images/mediasci.png" alt="">
           </a>. All Rights Reserved</p>
				
            </div>
        </div>
    </div>
</footer>
<script>
    $(document).ready(function () {
        $("#footer_quick_contact_form").on('submit', function (e) {
            var berrors = '';
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "<?= url('/contact-us') ?>",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                data: $("#footer_quick_contact_form").serialize(),
                success: function (response) {
                    if (JSON.parse(response).status == 'success')
                        alert('Contact Submitted successfully');
                    document.getElementById("footer_quick_contact_form").reset();
                },
                statusCode: {
                    500: function (response) {
                        alert(response.responseJSON.message);
                    },
                    422: function (response) {
                        berrors += response.responseJSON.message;
                        for (var er_in in response.responseJSON.errors) {
                            for (var er_me in response.responseJSON.errors[er_in]) {
                                berrors += "<br>" + response.responseJSON.errors[er_in][er_me];
                            }
                        }
                        alert(berrors);
                    },
                    419: function (response) {
                        alert("Sorry, session has expired. Login and try again");
                    }
                    ,
                    404: function (response) {
                        alert("Sorry, the page you are trying to reach is not found");
                    }
                }
            });
        });
    });
</script>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyDRt4t1aVecHVflkaMyDd8Xk3xagSXM8Bg"></script>
<script src="front/js/google-map-init-multilocation.js"></script>