<form class="form-inline">
    <input id='search' class="form-control mr-sm-2 position-relative  wow fadeIn" 
        data-wow-delay="0.3s" type="search" placeholder="Search" aria-label="Search">
</form>
@push('footer')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
          var availableTags = [
           @foreach($posts as $post)
            {
                label: "{!!$post->title!!}",
                value: "{{$post->slug}}"
            },
           @endforeach
          ];
          $( "#search" ).autocomplete({
                source: availableTags, 
                minLength: 2 ,
                select: function (e, ui) {
                    window.location = './'+ui.item.value;
                }
            });
        } );
        </script>
@endpush