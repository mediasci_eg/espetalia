<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="Medikal - Health & Medical HTML Template" />
    <meta name="keywords"
        content=" clinic, dental, doctor, health, hospital, medical, medical theme, medicine, therapy" />
    <meta name="author" content="ThemeMascot" />

    <!-- Facebook Tags -->
    <meta property="og:title" content="Neuro Espitalia specialized in neurosurgery and neurology">
    <meta property="og:description" content="Designed to meet all the requirements needed by physicians and patients alike in order to provide the best quality treatment and services possible.">
    <meta property="og:image" content="front/images/logo-wide-square.png">
    <meta property="og:url" content="https://neuroespitalia.com/">

    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <base href="{{url('')}}/">
    <!-- Page Title -->
    <title>Espitalia</title>

    <!-- Favicon and Touch Icons -->
    <link href="Neuro Logo PNG.png" rel="shortcut icon" type="image/png">
    <link href="front/images/apple-touch-icon.png" rel="icon">
    <link href="front/images/apple-touch-icon-72x72.png" rel="icon" sizes="72x72">
    <link href="front/images/apple-touch-icon-114x114.png" rel="icon" sizes="114x114">
    <link href="front/images/apple-touch-icon-144x144.png" rel="icon" sizes="144x144">

    <!-- Stylesheet -->
    <link href="front/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="front/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="front/css/animate.css" rel="stylesheet" type="text/css">
    <link href="front/css/css-plugin-collections.css" rel="stylesheet" />
    <!-- CSS | menuzord megamenu skins -->
    <link id="menuzord-menu-skins" href="front/css/menuzord-skins/menuzord-boxed.css" rel="stylesheet" />
    <!-- CSS | Main style file -->
    <link href="front/css/style-main.css" rel="stylesheet" type="text/css">
    <!-- CSS | Our style file -->
    <!-- <link href="front/css/our-style.css" rel="stylesheet" type="text/css">-->
    <!-- CSS | Preloader Styles -->
    <link href="front/css/preloader.css" rel="stylesheet" type="text/css">
    <!-- CSS | Custom Margin Padding Collection -->
    <link href="front/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
    <!-- CSS | Responsive media queries -->
    <link href="front/css/responsive.css" rel="stylesheet" type="text/css">
    <!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
    <!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

    <!-- Revolution Slider 5.x CSS settings -->
    <link href="front/js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css" />
    <link href="front/js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css" />
    <link href="front/js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css" />

    <!-- CSS | Theme Color -->
    <link href="front/css/colors/theme-skin-blue.css" rel="stylesheet" type="text/css">

    <!-- external javascripts -->

    <script src="front/js/jquery-2.2.4.min.js"></script>
    <script src="front/js/jquery-ui.min.js"></script>
    <script src="front/js/bootstrap.min.js"></script>
    <!-- JS | jquery plugin collection for this theme -->
    <script src="front/js/jquery-plugin-collection.js"></script>

    <!-- Revolution Slider 5.x SCRIPTS -->
    <script src="front/js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
    <script src="front/js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>


    @stack('header')
    <style>
        header .navbar-expand-lg .sec-menu .navbar-nav .nav-item {
            margin-right: 20px;
        }

        @media screen and (max-width: 1100px) {
            header .navbar-expand-lg .sec-menu .navbar-nav .nav-item {
                margin-right: 9px;
            }
        }
    </style>
</head>


<body>

    @include('front.layouts.header')

    @yield('content')

    @include('front.layouts.footer')
    <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>

    <script src="front/js/custom.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
              (Load Extensions only on Local File Systems ! 
               The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="front/js/revolution-slider/js/extensions/revolution.extension.actions.min.js">
    </script>
    <script type="text/javascript" src="front/js/revolution-slider/js/extensions/revolution.extension.carousel.min.js">
    </script>
    <script type="text/javascript" src="front/js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js">
    </script>
    <script type="text/javascript"
        src="front/js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="front/js/revolution-slider/js/extensions/revolution.extension.migration.min.js">
    </script>
    <script type="text/javascript"
        src="front/js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="front/js/revolution-slider/js/extensions/revolution.extension.parallax.min.js">
    </script>
    <script type="text/javascript"
        src="front/js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="front/js/revolution-slider/js/extensions/revolution.extension.video.min.js">
    </script>

    @stack('footer')

</body>

</html>