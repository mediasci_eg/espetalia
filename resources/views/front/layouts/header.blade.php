<header id="header" class="header">
    <div class="header-top bg-theme-colored sm-text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="widget no-border m-0">
                        <ul class="styled-icons icon-dark icon-theme-colored icon-sm sm-text-center">
                            <li><a href="{{$settings['facebook_link']}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="{{$settings['instagram_link']}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="widget no-border m-0">
                        <ul class="list-inline pull-right flip sm-pull-none sm-text-center mt-5">
                            <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-white"></i> <a class="text-white" href="javascript:void(0)">{{$settings['call_us']}}</a> </li> 
                            <li class="m-0 pl-10 pr-10"> <i class="fa fa-clock-o text-white"></i> <a class="text-white" href="javascript:void(0)">{{$post->data['day_1']}} {{$post->data['time_1']}}</a> </li>
                           <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-white"></i> <a class="text-white" href="mailto:{{$settings['email']}}">{{$settings['email']}}</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-nav">
        <div class="header-nav-wrapper navbar-scrolltofixed bg-lightest">
            <div class="container">
                <nav id="menuzord-right" class="menuzord blue bg-lightest">
                    <a class="menuzord-brand pull-left flip" href="">
                       <!-- <img src="front/images/logo-wide.png" alt="" style="
    height: 150px;
    margin: 8px 0;
">-->
                        <img src="front/images/logo-wide.png" alt="">
                    </a>
                    <ul class="menuzord-menu">
                        <li class="{{ Request::segment(1) == '' ? 'active' : '' }}"><a href="">Home</a>
                        </li>          
                        <li class="{{ Request::segment(1) === 'services' ? 'active' : '' }}"><a href="javascript:void(0)">Services</a>
                            <ul class="dropdown">
                                <?php if(isset($services)){
                                    foreach($services as $service){
                                        if($service->slug != "neuro-surgery"){
                                        ?>
                                        <li><a href="{{url($service->slug)}}">{{$service->title}}</a></li>
                                        <?php
                                    }}
                                }?>
                            </ul> 
                        </li>
                        <li class="{{ Request::segment(1) === 'doctors' ? 'active' : '' }}"><a href="{{url('/doctors')}}">Doctors</a>
                        </li>
                        <li class="{{ Request::segment(1) === 'surgical-specialty' ? 'active' : '' }}"><a href="{{url('surgical-specialty')}}">Surgical Specialty</a>
                        </li>
                        <li class="{{ Request::segment(1) === 'patient' ? 'active' : '' }}"><a href="{{url('/patient')}}">Patient Info</a>
                        </li>
                        <li class="{{ Request::segment(1) === 'schedule' ? 'active' : '' }}"><a href="{{url('/schedule')}}">Clinics Schedule</a>
                        </li>
                        <li class="{{ Request::segment(1) === 'news-events' ? 'active' : '' }}"><a href="{{url('/news-events')}}">News</a>
                        </li>
                        <li class="{{ Request::segment(1) === 'about' ? 'active' : '' }}"><a href="{{url('/about')}}">About Us</a>
                        </li> 
                        <li class="{{ Request::segment(1) === 'contact' ? 'active' : '' }}"><a href="{{url('/contact')}}">Contact Us</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
<style>
    .nav-link.active{
        color: #ffff00 !important;
    }
</style>

<?php

function menu($term) {
    if (is_array($term)) {
        foreach ($term as $item) {
            if (request()->segment(1) == $item) {
                return 'active';
                continue;
            }
        }
        return '';
    } elseif (request()->segment(1) == $term)
        return 'active';
    else
        return '';
}
?>