<div class='intro position-relative  wow fadeIn' style='visibility:hidden' data-wow-duration="2s">
    <div class='container'>
        <div class='row'>
            <div class='col-lg-8' style='z-index : 5;overflow: hidden;'>
                <h1 class=" wow fadeInUp "  style="padding: 15px 0 85px;"data-wow-duration="2s" data-wow-delay="0.7s">
                    <span><?= $title ?></span></h1>
            </div>
            <div class='img-wrap position-absolute wow fadeInUp ' data-wow-duration="2s" data-wow-delay="0.7s">
                <img src="<?= $image ?>" alt="">
            </div>
        </div>
    </div>
</div>
