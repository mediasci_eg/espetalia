@extends('front.layouts.master')
@section('content')

<div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-8" data-bg-img="http://placehold.it/1920x1280">
        <div class="container pt-60 pb-60">
            <!-- Section Content -->
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12 xs-text-center">
                        <h3 class="title text-white">Doctor Details</h3>
                        <ol class="breadcrumb mt-10 white">
                            <li><a class="text-white" href="">Home</a></li>
                            <li class="active text-theme-colored">Doctor Details</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Section: Doctor Details -->
    <section class="">
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-sx-12 col-sm-4 col-md-4">
                        <div class="doctor-thumb">
                            <img src="{{url('uploads/'.$doctor->image)}}" alt="">
                        </div>
<!--                        <div class="">
                            <div class="border-5px p-20">
                                <h5><i class="fa fa-clock-o text-theme-colored"></i> Opening Hours</h5>
                                <div class="opening-hours text-left">
                                    <ul class="list-unstyled">
                                        @for($i = 1 ; $i<=7 ; $i++)
                                        @if($post->data['day_'.$i])
                                        <li class="clearfix line-height-1"> <span>{{$post->data['day_'.$i]}}</span>
                                            <div class="value">{{$post->data['time_'.$i]}}</div>
                                        </li>
                                        @endif
                                        @endfor
                                    </ul>
                                </div>
                            </div>
                        </div>
                        Section Client's Feedback
                        <div class="mt-30">
                            <h4>Client's Feedback</h4>
                            <div class="owl-carousel-1col" data-dots="true">
                                @foreach($testimonials as $testimonial)
                                <div class="item">
                                    <div class="testimonial style1">
                                        <div class="comment">
                                            <p>{{$testimonial->content}}</p>
                                        </div>
                                        <div class="content mt-20">
                                            <div class="text-right flip pull-right flip mr-20 mt-10">
                                                <h5 class="author text-theme-colored">{{$testimonial->name}}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>-->
                    </div>
                    <h3 class="mt-0">{{$doctor->name}}</h3>
                    {!!$doctor->content!!}
                </div>
            </div>
        </div>
    </section>

    <!-- Divider: Divider -->
    <section class="divider parallax layer-overlay overlay-theme-colored-9" data-bg-img="http://placehold.it/1920x1280" data-parallax-ratio="0.7">
        <div class="container pt-30 pb-30">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12 text-center">
                        <h2 class="font-28 text-white">For Emergency Service Please Contact</h2>
                        <h3 class="font-30 text-white">{{$post->data['emergency_phone']}}</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Divider: Appoinment Form -->
    <section id="Appoinment" data-bg-img="images/pattern/p4.png">
        <div class="container pt-50 pb-0">
            <div class="row">
                <div class="col-md-6">
                    <div class="p-10">
                        <!-- Reservation Form Start-->
                        <form id="reservation_form" name="reservation_form" class="reservation-form" method="post" action="reservation"><h2 class="mt-0 line-bottom line-height-1 text-black mb-10">Make An Appoinment<span class="text-theme-colored font-weight-600"> Now!</span></h2>
                            <input type="hidden" type="text" name="doctor_id" value="{{$doctor->id}}" class="form-control">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group mb-10">
                                        <label>Name</label>
                                        <input placeholder="Enter Name" type="text" id="reservation_name" name="name" required class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group mb-10">
                                        <label>Email</label>
                                        <input placeholder="Email" type="email" id="reservation_email" name="email" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group mb-10">
                                        <label>Phone</label>
                                        <input placeholder="Phone" type="text" id="reservation_phone" name="phone" class="form-control" required>
                                    </div>
                                </div>
                                
                                <div class="col-sm-6">
                                    <div class="form-group mb-10">
                                        <label>Date</label>
                                        <input placeholder="Date" type="date" id="reservation_phone" name="date" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group mb-10">
                                        <label>Time</label>
                                        <input placeholder="Time" type="time" id="reservation_phone" name="time" class="form-control" required>
                                    </div>
                                </div>
                                
                                <div class="col-sm-12">
                                    <div class="form-group mb-10">
                                        <label>Message</label>
                                        <textarea id="form_message" name="message" class="form-control"  placeholder="Enter Your Enquiry" rows="5" aria-required="true" required></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group mb-0 mt-0">
                                        <!--<input name="form_botcheck" class="form-control" type="hidden" value="">-->
                                        <button type="submit" class="btn btn-theme-colored btn-lg btn-block">Submit Now</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="{{url('uploads/'.$doctor->image)}}" alt="">
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function () {
        $("#reservation_form").on('submit', function (e) {
            var berrors = '';
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "<?= url('/reservation') ?>",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                data: $("#reservation_form").serialize(),
                success: function (response) {
                    if (JSON.parse(response).status == 'success')
                        alert('Reservation Submitted successfully');
                    document.getElementById("reservation_form").reset();
                },
                statusCode: {
                    500: function (response) {
                        alert(response.responseJSON.message);
                    },
                    422: function (response) {
                        berrors += response.responseJSON.message;
                        for (var er_in in response.responseJSON.errors) {
                            for (var er_me in response.responseJSON.errors[er_in]) {
                                berrors += "<br>" + response.responseJSON.errors[er_in][er_me];
                            }
                        }
                        alert(berrors);
                    },
                    419: function (response) {
                        alert("Sorry, session has expired. Login and try again");
                    }
                    ,
                    404: function (response) {
                        alert("Sorry, the page you are trying to reach is not found");
                    }
                }
            });
        });
    });
</script>

@stop