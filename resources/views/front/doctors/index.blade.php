@extends('front.layouts.master')
@section('content')

<div class="main-content">
    <!-- Section: Doctors -->
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-8" data-bg-img="http://placehold.it/1920x1280">
        <div class="container pt-60 pb-60">
            <!-- Section Content -->
            <div class="section-content">
                <div class="row"> 
                    <div class="col-md-12 xs-text-center">
                        <h3 class="title text-white">Doctors</h3>
                        <ol class="breadcrumb mt-10 white">
                            <li><a class="text-white" href="">Home</a></li>
                            <li class="active text-theme-colored">Doctors</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="section-title text-center">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="text-uppercase mt-0 line-height-1">Our Doctors</h2>
                        <div class="title-icon">
                            <img class="mb-10" src="images/title-icon.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-content">
              
                    <?php $counter = 0;?>
                    @foreach($doctors as $doctor)
                    <?php 
                    $counter++;
                    if($counter == 1){
                        ?>
                        <div class="row multi-row-clearfix">
                        <?php
                       
                    }?>
                    <div class="col-sm-6 col-md-3 sm-text-center mb-30">
                        <div class="team maxwidth400">
                            <div class="thumb"><img class="img-fullwidth" src="{{url('uploads/'.$doctor->image)}}" alt=""></div>
                            <div class="content border-1px p-15 bg-light clearfix">
                                <h4 class="name text-theme-color-2 mt-0"mail
                                >{{$doctor->name}}</h4>
                                <p class="mb-20">{{$doctor->title}}</p>
                                <a class="btn btn-theme-colored btn-sm pull-left flip" href="{{url('/doctors/'.$doctor->id)}}">view details</a>
                            </div>
                        </div>
                    </div>
                    <?php 
                    
                    if($counter == 4){
$counter =0;
?>
</div>
<?php 
                    }
                    ?>
                    @endforeach
                
            </div>
        </div>
    </section>
</div>

@stop