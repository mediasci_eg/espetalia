@extends('front.layouts.master')
@section('content')

<div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-8" data-bg-img="http://placehold.it/1920x1280">
        <div class="container pt-60 pb-60">
            <!-- Section Content -->
            <div class="section-content">
                <div class="row"> 
                    <div class="col-md-12 xs-text-center">
                        <h3 class="title text-white">Contact Us</h3>
                        <ol class="breadcrumb mt-10 white">
                            <li><a class="text-white" href="">Home</a></li>
                            <li class="active text-theme-colored">Contact</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Divider: Contact -->
    <section class="divider">
        <div class="container pt-sm-10 pb-sm-30">
            <div class="row pt-30">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="icon-box left media bg-deep p-30 mb-20"> <a class="media-left pull-left" href="javascript:void(0)"> <i class="pe-7s-map-2 text-theme-colored"></i></a>
                                <div class="media-body"> <strong>OUR OFFICE LOCATION</strong>
                                    <p>{{$settings['address']}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="icon-box left media bg-deep p-30 mb-20"> <a class="media-left pull-left" href="javascript:void(0)"> <i class="pe-7s-call text-theme-colored"></i></a>
                                <div class="media-body"> <strong>OUR CONTACT NUMBER</strong>
                                    <p>{{$settings['call_us']}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="icon-box left media bg-deep p-30 mb-20"> <a class="media-left pull-left" href="javascript:void(0)"> <i class="pe-7s-mail text-theme-colored"></i></a>
                                <div class="media-body"> <strong>OUR CONTACT E-MAIL</strong>
                                    <p>{{$settings['email']}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="icon-box left media bg-deep p-30 mb-20"> <a class="media-left pull-left" href="javascript:void(0)"> <i class="pe-7s-shield text-theme-colored"></i></a>
                                <div class="media-body"> <strong>OUR EMERGENCY NUMBER</strong>
                                    <p>{{$post->data['emergency_phone']}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <h2 class="mt-0 mb-20 line-height-1">Interested in discussing?</h2>
                    <!-- Contact Form -->
                    <form id="contact_form" name="contact_form" class="" action="contact-us" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="form_name">Name <small>*</small></label>
                                    <input id="form_name" name="name" class="form-control" type="text" placeholder="Enter Name" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="form_email">Email <small>*</small></label>
                                    <input id="form_email" name="mail" class="form-control email" type="email" placeholder="Enter Email" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="form_subject">Subject <small>*</small></label>
                                    <input id="form_subject" name="subject" class="form-control" type="text" placeholder="Enter Subject" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="form_phone">Phone <small>*</small></label>
                                    <input id="form_phone" name="phone" pattern=".{11,11}" title="it must be 11 number"  onkeypress="return isNumber(event)" class="form-control" type="text" placeholder="Enter Phone" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="form_message">Message <small>*</small></label>
                            <textarea id="form_message" name="message" class="form-control" rows="5" placeholder="Enter Message" required></textarea>
                        </div>
                        <div class="form-group">
                            <!--<input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="" />-->
                            <button type="submit" class="btn btn-dark btn-theme-colored btn-flat mr-5">Send your message</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-5">

<!--             Google Map HTML Codes 
-->            <div 
              data-address="121 King Street, Melbourne Victoria 3000 Australia"
              data-popupstring-id="#popupstring1"
              class="map-canvas autoload-map"
              data-mapstyle="style2"
              data-height="500"
              data-latlng="-37.817314,144.955431"
              data-title="sample title"
              data-zoom="12"
              data-marker="images/map-marker.png">
            </div>
            <div class="map-popupstring hidden" id="popupstring1">
              <div class="text-center">
                <h3>ThemeMascot Office</h3>
                <p>121 King Street, Melbourne Victoria 3000 Australia</p>
              </div>
            </div>
             Google Map Javascript Codes 
            <script src="http://maps.google.com/maps/api/js?key=AIzaSyAYWE4mHmR9GyPsHSOVZrSCOOljk8DU9B4"></script>
            <script src="front/js/google-map-init.js"></script>

          </div>
            </div>
        </div>
    </section>

</div>
<script>
    $(document).ready(function () {
        $("#contact_form").on('submit', function (e) {
            var berrors ='';
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "<?= url('/contact-us') ?>",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                data: $("#contact_form").serialize(),
                success: function (response) {
                    if (JSON.parse(response).status == 'success')
                        alert('Contact Submitted successfully');
                    document.getElementById("contact_form").reset();
                },
                       statusCode:{
                           500:function(response){
                             alert(response.responseJSON.message);
                           },
                           422:function(response){
                             berrors += response.responseJSON.message;
                             for(var er_in in response.responseJSON.errors){
                                 for(var er_me in response.responseJSON.errors[er_in]){
                                    berrors += "<br>"+response.responseJSON.errors[er_in][er_me];
                                 }
                             }
                             alert(berrors);
                           },
                           419:function(response){
                            alert("Sorry, session has expired. Login and try again");
                           }
                           ,
                           404:function(response){
                            alert("Sorry, the page you are trying to reach is not found");
                           }
                       }
            });
        });
    });
</script>
@stop