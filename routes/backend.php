<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::group(['prefix' => 'backend', 'namespace' => 'Backend', 'middleware' => ['auth:admin']], function () {
    Route::get('/', 'HomeController@index');

    // ############################ Slider Crud ##############################
    Route::get('sliders', 'SlidersController@index');
    Route::get('sliders/create', 'SlidersController@create');
    Route::get('sliders/update/{id}', 'SlidersController@update');
    Route::post('sliders/delete/{slider}', 'SlidersController@delete');
    Route::post('sliders/store', 'SlidersController@store');

    // ############################ Department Crud ##############################
    Route::get('departments', 'DepartmentsController@index');
    Route::get('departments/create', 'DepartmentsController@create');
    Route::get('departments/update/{id}', 'DepartmentsController@update');
    Route::post('departments/delete/{department}', 'DepartmentsController@delete');
    Route::post('departments/store', 'DepartmentsController@store');

    // ############################ Doctor Crud ##############################
    Route::get('doctors', 'DoctorsController@index');
    Route::get('doctors/create', 'DoctorsController@create');
    Route::get('doctors/update/{id}', 'DoctorsController@update');
    Route::post('doctors/delete/{doctor}', 'DoctorsController@delete');
    Route::post('doctors/store', 'DoctorsController@store');

    // ############################ Reservation Crud ##############################
    Route::get('reservations', 'ReservationsController@index');
    Route::post('reservations/search', 'ReservationsController@search');
    Route::post('reservations/delete/{reservation}', 'ReservationsController@delete');
    Route::post('reservations/change-status', 'ReservationsController@reply');

    // ############################ Testimonials Crud ##############################
    Route::get('testimonials', 'TestimonialsController@index');
    Route::get('testimonials/create', 'TestimonialsController@create');
    Route::get('testimonials/update/{id}', 'TestimonialsController@update');
    Route::post('testimonials/delete/{testimonial}', 'TestimonialsController@delete');
    Route::post('testimonials/store', 'TestimonialsController@store');

    // ############################ Appointments Crud ##############################
    Route::get('appointments', 'AppointmentsController@index');
    Route::post('appointments/search', 'AppointmentsController@search');
    Route::post('appointments/delete/{appointment}', 'AppointmentsController@delete');
    Route::post('appointments/change-status', 'AppointmentsController@reply');

    // ############################ Settings Crud ##############################
    Route::get('settings', 'SettingsController@anyIndex');
    Route::post('settings/update_settings', 'SettingsController@anyUpdate');

    // ############################ Posts Crud ##############################
    Route::get('posts', 'PostsController@index');
    Route::get('posts/create', 'PostsController@create');
    Route::get('posts/update/{post}', 'PostsController@update');
    Route::post('posts/delete/{post}', 'PostsController@delete');
    Route::post('posts/store', 'PostsController@store');

    // ############################ News&Events Crud ##############################
    Route::get('news-events', 'NewsEventsController@index');
    Route::get('news-events/create', 'NewsEventsController@create');
    Route::get('news-events/update/{news_events}', 'NewsEventsController@update');
    Route::post('news-events/delete/{news_events}', 'NewsEventsController@delete');
    Route::post('news-events/store', 'NewsEventsController@store');


    // ############################ Contact-Us Crud ##############################
    Route::get('contact-us', 'ContactUsController@index');
    Route::post('contact-us/delete/{contact}', 'ContactUsController@delete');
    Route::post('contact-us/change-status', 'ContactUsController@reply');
    Route::post('contact-us/send', 'ContactUsController@sendEmail');


    Route::any('backend/ajax/upload', 'Helpers\AjaxController@anyUpload');
    Route::any('backend/ajax/editorupload', 'Helpers\AjaxController@anyEditorupload');
    Route::any('backend/ajax/removeimage', 'Helpers\AjaxController@anyRemoveimage');
    Route::any('backend/ajax/browse', 'Helpers\AjaxController@anyBrowse');
    Route::any('upload-image', 'UploadImageController@index');


    Route::post('upload_image/{name}', 'PostsController@galleryUpload');
});
