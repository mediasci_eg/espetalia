<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/files', function () {
    return ;
    $files = glob('C:\xampp\htdocs\herts\resources\views\front\posts/*' );
    foreach($files as $file){
        if(basename($file)=='New folder')continue ;
        if(basename($file)=='New folder (2)')continue ;
        $slug = explode('.blade', basename($file))[0] ;

        $fileContent = file_get_contents($file);
        
        $dom = new \DOMDocument;
        @$dom->loadHTML($fileContent);

        $xpath = new DOMXPath($dom);
        
        $xpath_resultset =  $xpath->query("//div/div");
        $content = $dom->saveHTML($xpath_resultset->item(0));
        
        $xpath_resultset =  $xpath->query("//div");
        
        $class = $xpath_resultset->item(0)->getAttribute('class') ;
        
        $title = explode('$title = \'' , $fileContent ) ;
        $title = explode('\';' , $title[1] )[0] ;
        
        $image = explode('$image = \'' , $fileContent ) ;
        $image = explode('\';' , $image[1] )[0] ;
        
       $post = new \App\Models\Post ;
       $post->title = $title ;
       $post->slug = $slug ;
       $post->content = $content ;
       $post->image = $image ;
       $post->class = $class ;
       $post->save() ;

    }
    echo 'done';
});


Route::group(['namespace' => 'Front'], function () {

    Route::get('pull', 'PullController@index');
    Route::get('schedule', 'PostsController@schedule');
    Route::post('contact-us', 'ContactUsController@createContact');
    Route::get('news-events', 'NewsEventsController@index');
    Route::get('news-events-detail/{id}', 'NewsEventsController@details');
    Route::get('contact', 'ContactUsController@index');
    Route::get('departments/{id}', 'DepartmentsController@index');
    Route::get('doctors', 'DoctorsController@index');
    Route::get('doctors/{id}', 'DoctorsController@details');
    Route::post('reservation', 'DoctorsController@reservation');
    Route::post('make-appointment', 'AppointmentController@index');
    Route::get('/{post}', 'PostsController@index');
    Route::get('/', 'HomeController@index');
});
