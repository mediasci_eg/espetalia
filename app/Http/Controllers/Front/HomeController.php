<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use App\Models\Post;
use App\Models\Testimonials;
use App\Models\Doctor;
use App\Models\Department;

class HomeController extends Controller {

    public function index() {
        $data['sliders'] = Slider::all();
        $data['doctors'] = Doctor::all();
        $data['departments'] = Department::all();
        // $data['testimonials'] = Testimonials::all();
        return view('front.home', $data);
    }

}
