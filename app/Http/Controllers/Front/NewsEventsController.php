<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\NewsEvents;

class NewsEventsController extends Controller {

    public function index() {
        $data['news_events'] = NewsEvents::orderBy('id','DESC')->get();
//        $data['featured'] = NewsEvents::Where('is_featured', 1)->take(4)->get();
//        $data['not_featured'] = NewsEvents::Where('is_featured', 0)->get();
        return view('front.news-events.index', $data);
    }

    public function details($id) {
        $data['item'] = $item = NewsEvents::findOrFail($id);
//        if ($item->type == 'News')
            return view('front.news-events.news-details', $data);
//        else
//            return view('front.news-events.events-details', $data);
    }

}
