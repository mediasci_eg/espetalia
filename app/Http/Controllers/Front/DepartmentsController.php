<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Elsayednofal\MediaManager\Controllers\MediaController;
use App\Models\Department;
use App\Models\Doctor;

class DepartmentsController extends Controller {

    function index($id) {
        $data['department'] = Department::where('id',$id)->first();
        $data['doctors'] = Doctor::where('department_id',$id)->get();
        return view('front.departments', $data);
    }

    function uploadCvs(Request $request) {
        $mimes = ['pdf', 'doc'];
        $file = $request->file('file_name');
        if (!in_array($file->getClientOriginalExtension(), $mimes))
            return redirect()->back()->withErrors('File Type is not Supported');
        $cv = new Cvs;
        $cv->job_id = $request->job_id;
        $cv->file_name = $file->getClientOriginalName();
        $destinationPath = public_path('/cvs');
        $file->move($destinationPath, $cv->file_name);
//        dd($cv);
        $cv->save();
        return redirect()->back()->with('success', 'CV Submitted Successfully');
    }

}
