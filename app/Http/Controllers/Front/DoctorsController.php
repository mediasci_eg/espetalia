<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Elsayednofal\MediaManager\Controllers\MediaController;
use App\Models\Doctor;
use App\Models\Testimonials;
use App\Models\Reservations;

class DoctorsController extends Controller {

    function index() {
        $data['doctors'] = Doctor::all();
        return view('front.doctors.index', $data);
    }

    function details($id) {
        $data['testimonials'] = Testimonials::all();
        $data['doctor'] = Doctor::where('id', $id)->first();
        return view('front.doctors.details', $data);
    }

    function reservation(Request $request) {
        $reservation = new Reservations;
        $response = [];
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'doctor_id' => 'required',
            'date' => 'required',
            'time' => 'required',
            'message' => 'required',
        ]);
        $reservation->fill($request->all());
        $reservation->save();
        $response["status"] = "success";
        return json_encode($response);
    }

}
