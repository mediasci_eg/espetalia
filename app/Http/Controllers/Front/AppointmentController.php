<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MakeAppointment;

class AppointmentController extends Controller {

    public function index(Request $request) {
        $appointment = new MakeAppointment;
        $response = [];
        $request->validate([
            'department_id' => 'required',
            'full_name' => 'required',
            'phone' => 'required',
            'date' => 'required',
        ]);
        $appointment->fill($request->all());
        $appointment->save();
        $response["status"] = "success";
        return json_encode($response);
    }

}
