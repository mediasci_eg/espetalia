<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;

class PostsController extends Controller {

    public function index($slug) {
        $data['service_post'] = $post = Post::whereSlug($slug)->firstOrFail();


        if ($post->is_parent == 1) {
            $data['tabs'] = Post::where('parent_id', $post->id)->get();
            //  dd($data['tabs']);
        }

        if ($slug != "about" && $slug != "patient") {
            $data["all_services"] = Post::where('parent_id', $post->parent_id)->get();
            $file_name = 'front.posts.exceptions.service-post';
            if (view()->exists($file_name)){
                return view($file_name, $data);
            }
        }
        
        $file_name = 'front.posts.exceptions.' . $post->slug;
        if (view()->exists($file_name))
            return view($file_name, $data);

        return view('front.posts.index', $data);
    }

    function schedule(){
        return view('front.posts.schedule');
    }

}
