<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ContactUs;

class ContactUsController extends Controller {

    public function index(Request $request) {
        return view('front.contact');
    }

    public function createContact(Request $request) {
        $contact = new ContactUs;
        $response = [];
        $request->validate([
        'name' => 'required',
        'mail' => 'required',
        'phone' => 'required',
        'subject' => 'required',
        'message' => 'required',
    ]);
        $contact->fill($request->all());
        $contact->save();
        $response["status"] = "success";
        return json_encode($response);
    }

}
