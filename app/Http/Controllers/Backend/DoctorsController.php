<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Elsayednofal\MediaManager\Controllers\MediaController;
use App\Models\Doctor;
//use App\Models\Department;
//use App\Models\DoctorsDepartments;
use DB;

class DoctorsController extends Controller {

    function index(Request $request) {
        $doctors = Doctor::orderBy('id');
        if ($request->name) {
            $doctors = Doctor::where('name', 'LIKE', '%' . $request->name . '%');
        }
        $data['result'] = $doctors->paginate(10);
        return view('backend.doctors.index', $data);
    }

    function create(Request $request) {
//        $data['departments'] = Department::all();
        $data['model'] = $model = new Doctor;
//        $data['doctors_departments'] = $doctorsDepartments = new DoctorsDepartments;
        return view('backend.doctors.create', $data);
    }

    function update(Request $request, $id) {
//        $data['departments'] = Department::all();
        $data['model'] = $model = Doctor::find($id);
//        $data['doctors_departments'] = $doctorsDepartments = DoctorsDepartments::where('doctor_id', $id)->first();
//        dd($data['doctors_departments']);
        return view('backend.doctors.update', $data);
    }

    function store(Request $request) {

        DB::beginTransaction();
        try {
            $model = Doctor::findOrNew($request->id);
            $model->fill($request->all());
            if ($request->hasFile('image') && $request->image != null) {
                $image = $this->imageUpload($request, 'image');
                if ($image)
                    $model->image = $image;
                else
                    return redirect()->back()->withErrors('Image Type is not Supported');
            }
            if (!$model->validate()) {
                return redirect()->back()->withErrors($model->errors())->withInput();
            }
            $model->save();
//            $doctorsDepartments = DoctorsDepartments::where('doctor_id', $request->id)->first();
//            if ($doctorsDepartments != null) {
               // $doctorsDepartments = DoctorsDepartments::where('doctor_id', $request->id)->update(['doctor_id' => $model->id, 'department_id' => $model->department_id]);
//            } else {
                /*$doctors_departments = new DoctorsDepartments;
                $doctors_departments->doctor_id = $model->id;
                $doctors_departments->department_id = $model->department_id;
                $doctors_departments->save();*/
//            }
            DB::commit();
            return redirect()->back()->with('success', 'Doctor Saved Successfully');
        } catch (\Exception $exception) {
            throw $exception;
            DB::rollBack();
            return back();
        }
    }

    function delete(Request $request, Doctor $doctor) {
        $doctor->delete();
        return response()->json(['message' => 'Doctor deleted successfully']);
    }

}
