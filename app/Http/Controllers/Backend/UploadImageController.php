<?php
namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Request ;
class UploadImageController extends Controller
{
	public function index(Request $request) {
        $image = request()->file('file');
        $input['file'] = $image;
        $v = \Validator::make($input, [
			'file' => 'image|mimes:jpeg,png,jpg,gif',
        ]);
		
		if(!in_array($image->extension() , ['jpeg','png','jpg','gif']))
			return false ;

        if ($v->fails()) {
            $errors = $v->errors()->toArray();
            $return = ['status' => '0', 'msg' => $errors['file']];
            echo json_encode($return);
            die;
        }

        //$name = md5(microtime()) . '.' . $image->getClientOriginalExtension();
        $name = md5(microtime()) . '.' . $image->extension();
        $image->move(public_path('uploads'), $name);

        $return = ['status' => '1', 'file' => $name];
        echo json_encode($return);
        die;
    }

}
