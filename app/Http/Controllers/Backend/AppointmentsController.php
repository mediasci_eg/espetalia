<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Elsayednofal\MediaManager\Controllers\MediaController;
use App\Models\MakeAppointment;
use DB;

class AppointmentsController extends Controller {

    function index(Request $request) {
        $data['trashed'] = MakeAppointment::onlyTrashed()->get();
        $appointments = MakeAppointment::orderBy('id', 'desc');
        if ($request->search) {
            $appointments = $this->search($request, $appointments);
        }
        $data['appointments'] = $appointments->paginate(10);
        return view('backend.appointments.index', $data);
    }

    function search(Request $request, $appointments) {
        foreach ($request->user as $key => $value) {
            if ($key == 'page' || $key == 'search' || $value == '')
                continue;

            if (is_numeric($value)) {
                $appointments = $appointments->where($key, $value);
            } else {
                $appointments = $appointments->where($key, 'like', '%' . $value . '%');
            }
        }
        return $appointments;
    }

    function reply(Request $request) {
        $reply = MakeAppointment::where('id', $request->reply_id)->first();
        if ($reply->is_reply == 0)
            MakeAppointment::where('id', $request->reply_id)->update(['is_reply' => 1]);
        else
            MakeAppointment::where('id', $request->reply_id)->update(['is_reply' => 0]);
    }

    function delete(Request $request, MakeAppointment $appointment) {
        $appointment->delete();
        return response()->json(['message' => 'Appointment deleted successfully']);
    }

}
