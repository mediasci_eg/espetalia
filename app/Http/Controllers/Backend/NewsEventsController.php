<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Elsayednofal\MediaManager\Controllers\MediaController;
use App\Models\NewsEvents;
use DB;

class NewsEventsController extends Controller {

    function index(Request $request) {
        $news_events = NewsEvents::orderBy('id', 'DESC');
        if ($request->title) {
            $news_events = NewsEvents::where('title', 'LIKE', $request->title . '%');
//            dd($news_events);
//            $news_events = $this->search($request, $news_events);
        }
        $data['result'] = $news_events->paginate(20);
        return view('backend.news-events.index', $data);
    }

    function create(Request $request) {
        $data['model'] = $model = new NewsEvents;
        return view('backend.news-events.create', $data);
    }

    function update(Request $request, $id) {
        $data['model'] = $model = NewsEvents::find($id);
        return view('backend.news-events.update', $data);
    }

    function store(Request $request) {
        DB::beginTransaction();
        try {

            $model = NewsEvents::findOrNew($request->id);
            $model->fill($request->all());
            
            //Image
            if ($request->hasFile('image') && $request->image != null) {
                $image = $this->imageUpload($request, 'image');
                if ($image)
                    $model->image = $image;
                else
                    return redirect()->back()->withErrors('Image Type is not Supported');
            }

            $model->slug = str_slug($request->title);
//            if ($request->get('is_featured'))
//                $model->is_featured = 1;
//            else
//                $model->is_featured = 0;
            if (!$model->validate()) {
                session()->flash('errors', $model->errors());
                return back();
            }
            $model->save();
            DB::commit();
           return redirect()->back()->with('success', 'News Saved Successfully');
        } catch (\Exception $exception) {
           throw $exception;
            DB::rollBack();
            return back();
        }
    }

    function delete(Request $request, NewsEvents $news_events) {
        $news_events->delete();
        return response()->json(['message' => 'News deleted successfully']);
    }

}
