<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Post;
use App\Models\Setting;
use App\Models\Department;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    function __construct() {
        
    }

    public function imageUpload($request, $name) {
        $this->validate($request, [
            $name => 'required|image|mimes:jpeg,png,jpg,gif',
        ]);
        $mimes = ['jpeg', 'JPEG', 'png', 'PNG', 'jpg', 'JPG', 'gif', 'GIF'];
        $image = $request->file($name);
        if (!in_array($image->getClientOriginalExtension(), $mimes))
            return false;

        $name = $name . '_' . time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads');
        $image->move($destinationPath, $name);
        return $name;
    }

}
