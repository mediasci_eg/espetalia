<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;
use App\Models\Post;
use App\Models\postsImages;
use Illuminate\Http\Request;

class PostsController extends Controller
{

    public function index(Request $request)
    {
        $posts = Post::where('parent_id', null)
            ->orderBy('updated_at', 'DESC')->orderBy('id', 'DESC');
        if ($request->search) {
            $posts = $this->search($request, $posts);
        }
        $data['posts'] = $posts->paginate(20);
        return view('backend.posts.index', $data);
    }

    // function indexBusiness($post_id)
    // {
    //     $posts = Post::where('parent_id',$post_id);
    //     $data['posts'] = $posts->paginate(20);
    //     return view('backend.posts.index', $data);
    // }

    public function search(Request $request, $users)
    {
        foreach ($request->user as $key => $value) {
            if ($key == 'page' || $key == 'search' || $value == '') {
                continue;
            }

            if (is_numeric($value)) {
                $users = $users->where($key, $value);
            } else {
                $users = $users->where($key, 'like', $value . '%');
            }
        }
        return $users;
    }

    public function create(Request $request)
    {
        return view('backend.posts.create')->with('post', new Post);
    }

    public function update(Request $request, Post $post)
    {
        if ($post->is_parent == 1) {

            $posts = Post::where('parent_id', $post->id);

            $data['posts'] = $posts->paginate(20);
            return view('backend.posts.index', $data);
        }
//        dd($post);
        return view('backend.posts.update')->with('post', $post);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            //'content' => 'required',
            //'image' => 'required',
        ]);

        $post = Post::findOrNew($request->id);
        $post->title = $request->title;
        $post->content = $request->content;
        if ($request->hasFile('image') && $request->image != null) {
            $image = $this->imageUpload($request, 'image');
            if ($image) {
                $post->image = $image;
            } else {
                return redirect()->back()->withErrors('Image Type is not Supported');
            }

        }

        if ($request->data != '') {
            $post->data = $request->data;
        }

        $post->save();

        postsImages::where("post_id", $request->id)->delete();
        if (isset($request->images)) {
            foreach ($request->images as $oneimage) {
                $post_image = new postsImages();
                $post_image->post_id = $post->id;
                $post_image->image = $oneimage;
                $post_image->save();
            }

        }

        return redirect('backend/posts')->with('success', 'Data Saved Successfully');
    }

    public function delete(Request $request, User $user)
    {
        $user->delete();
        return response()->json(['message' => 'user deleted successfully']);
    }

    public function galleryUpload(Request $request, $name)
    {
        if (!file_exists('uploads')) {
            mkdir('uploads', 0777);
        }
        $this->validate($request, [
            $name => 'required|image|mimes:jpeg,png,jpg,gif',
        ]);
        $mimes = ['jpeg', 'JPEG', 'png', 'PNG', 'jpg', 'JPG', 'gif', 'GIF'];
        $image = $request->file($name);
        if (!in_array($image->getClientOriginalExtension(), $mimes)) {
            return false;
        }

        $name = md5($image->getClientOriginalName() . '_' . microtime()) . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads');
        $image->move($destinationPath, $name);
        return response()->json([
            'name' => $name,
        ]);
    }

}
