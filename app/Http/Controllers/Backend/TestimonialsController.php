<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Elsayednofal\MediaManager\Controllers\MediaController;
use App\Models\Testimonials;
use DB;

class TestimonialsController extends Controller {

    function index(Request $request) {
        $testimonials = Testimonials::orderBy('id', 'DESC');
        if ($request->name) {
            $testimonials = Testimonials::where('name', 'LIKE', '%' . $request->name . '%');
        }
        $data['result'] = $testimonials->paginate(10);
        return view('backend.testimonials.index', $data);
    }

    function create(Request $request) {
        $data['model'] = $model = new Testimonials;
        return view('backend.testimonials.create', $data);
    }

    function update(Request $request, $id) {
        $data['model'] = $model = Testimonials::find($id);
        return view('backend.testimonials.update', $data);
    }

    function store(Request $request) {
        DB::beginTransaction();
        try {
            $model = Testimonials::findOrNew($request->id);
            $model->fill($request->all());
            if (!$model->validate()) {
                session()->flash('errors', $model->errors());
                return back();
            }

            $model->save();
            DB::commit();
            return redirect()->back()->with('success', 'Testimonial Saved Successfully');
        } catch (\Exception $exception) {
            throw $exception;
            DB::rollBack();
            return back();
        }
    }

    function delete(Request $request, Testimonials $testimonial) {
        $testimonial->delete();
        return response()->json(['message' => 'Testimonial deleted successfully']);
    }

}
