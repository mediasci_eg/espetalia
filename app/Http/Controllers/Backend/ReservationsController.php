<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reservations;

class ReservationsController extends Controller {

    function index(Request $request) {
        $data['trashed'] = Reservations::onlyTrashed()->get();
        $applicants = Reservations::orderBy('id', 'desc');
        if ($request->search) {
            $applicants = $this->search($request, $applicants);
        }
        $data['applicants'] = $applicants->paginate(10);
        return view('backend.reservations.index', $data);
    }

    function search(Request $request, $applicants) {
        foreach ($request->user as $key => $value) {
            if ($key == 'page' || $key == 'search' || $value == '')
                continue;

            if (is_numeric($value)) {
                $applicants = $applicants->where($key, $value);
            } else {
                $applicants = $applicants->where($key, 'like', '%' . $value . '%');
            }
        }
        return $applicants;
    }

    function reply(Request $request) {
        $reply = Reservations::where('id', $request->reply_id)->first();
        if ($reply->is_reply == 0)
            Reservations::where('id', $request->reply_id)->update(['is_reply' => 1]);
        else
            Reservations::where('id', $request->reply_id)->update(['is_reply' => 0]);
    }

    function delete(Request $request, Reservations $reservation) {
        $reservation->delete();
        return response()->json(['message' => 'Reservation deleted successfully']);
    }

}
