<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use MediaSci\Helpers\controllers\Mail;

class ContactUsController extends Controller {

    function index(Request $request) {
        $data['trashed'] = ContactUs::onlyTrashed()->get();
        $data['contacts'] = $contacts = ContactUs::orderBy('id', 'desc');

        if ($request->search) {
            $contacts = $this->search($request, $contacts);
        }

        $data['contacts'] = $contacts->paginate(10);

        return view('backend.contact-us.index', $data);
    }

    function search(Request $request, $contacts) {
        foreach ($request->user as $key => $value) {
            if ($key == 'page' || $key == 'search' || $value == '')
                continue;

            if (is_numeric($value)) {
                $contacts = $contacts->where($key, $value);
            } else {
                $contacts = $contacts->where($key, 'like', '%' . $value . '%');
            }
        }
        return $contacts;
    }

    function reply(Request $request) {
        $reply = ContactUs::where('id', $request->reply_id)->first();
        if ($reply->is_reply == 0)
            ContactUs::where('id', $request->reply_id)->update(['is_reply' => 1]);
        else
            ContactUs::where('id', $request->reply_id)->update(['is_reply' => 0]);
    }

    function sendEmail(Request $request) {
        $contact = ContactUs::where('id', $request->contact_id)->first();
        return Mail::send(config('backend-users.mail_from'), $contact->mail, $request->contact_subject, $request->contact_message);
        ContactUs::where('id',$request->contact_id)->update(['is_reply'=>1]);
//        $response["replied"] = "This has been replied";
//        return json_encode($response);
    }

    function delete(Request $request, ContactUs $contact) {
        $contact->delete();
        return response()->json(['message' => 'Contact deleted successfully']);
    }

}
