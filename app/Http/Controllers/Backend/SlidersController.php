<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use Elsayednofal\MediaManager\Controllers\MediaController;
use App\Models\Slider;
use DB;

class SlidersController extends Controller {

    function index(Request $request) {
        $sliders = Slider::orderBy('id', 'DESC');
        if ($request->name) {
            $sliders = Slider::where('name', 'LIKE', '%' . $request->name . '%');
        }
        $data['result'] = $sliders->paginate(10);
        return view('backend.sliders.index', $data);
    }

    function create(Request $request) {
        $data['model'] = $model = new Slider;
        return view('backend.sliders.create', $data);
    }

    function update(Request $request, $id) {
        $data['model'] = $model = Slider::find($id);
        return view('backend.sliders.update', $data);
    }

    function store(Request $request) {
        DB::beginTransaction();
        try {

            $model = Slider::findOrNew($request->id);
            $model->fill($request->all());
            
            //Image
            if ($request->hasFile('image') && $request->image != null) {
                $image = $this->imageUpload($request, 'image');
                if ($image)
                    $model->image = $image;
                else{
                    return redirect()->back()->withErrors('Image Type is not Supported');
            }
            }

            if (!$model->validate()) {
                session()->flash('errors', $model->errors());
                return back()->withInput();
            }
            $model->save();
            DB::commit();
           return redirect()->back()->with('success', 'Slider Saved Successfully');
        } catch (\Exception $exception) {
           throw $exception;
            DB::rollBack();
            return back();
        }
    }

    function delete(Request $request, Slider $slider) {
        $slider->delete();
        return response()->json(['message' => 'Slider deleted successfully']);
    }

}
