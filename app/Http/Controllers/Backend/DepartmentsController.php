<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Elsayednofal\MediaManager\Controllers\MediaController;
use App\Models\Department;
use DB;

class DepartmentsController extends Controller {

    function index(Request $request) {
        $departments = Department::orderBy('id', 'DESC');
        if ($request->name) {
            $departments = Department::where('name', 'LIKE', '%' . $request->name . '%');
        }
        $data['result'] = $departments->paginate(10);
        return view('backend.departments.index', $data);
    }

    function create(Request $request) {
        $data['model'] = $model = new Department;
        return view('backend.departments.create', $data);
    }

    function update(Request $request, $id) {
        $data['model'] = $model = Department::find($id);
        return view('backend.departments.update', $data);
    }

    function store(Request $request) {
        DB::beginTransaction();
        try {
            $model = Department::findOrNew($request->id);
            $model->fill($request->all());
            if ($request->hasFile('image') && $request->image != null) {
                $image = $this->imageUpload($request, 'image');
                if ($image)
                    $model->image = $image;
                else
                    return redirect()->back()->withErrors('Image Type is not Supported');
            }
            if (!$model->validate()) {
                session()->flash('errors', $model->errors());
                return back()->withInput();
            }

            $model->save();
            DB::commit();
            return redirect()->back()->with('success', 'Department Saved Successfully');
        } catch (\Exception $exception) {
            throw $exception;
            DB::rollBack();
            return back();
        }
    }

    function delete(Request $request, Department $department) {
        $department->delete();
        return response()->json(['message' => 'Department deleted successfully']);
    }

}
