<?php

namespace App\Http\Controllers\Helpers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use Intervention\Image\Image;
use Intervention\Image\Facades\Image;

class Functions extends Controller
{

    public static function selectedPost($name, $value, $option2)
    {
        if(isset($_POST[$name]))
        {
            if($_POST[$name] == $option2)
            {
                return "selected='selected'";
            }
        }
        else
        {
            if($value == $option2)
            {
                return "selected='selected'";
            }
        }
    }

    public static function selectedGet($name, $value, $option2)
    {
        if(isset($_GET[$name]))
        {
            if($_GET[$name] == $option2)
            {
                return "selected='selected'";
            }
        }
        else
        {
            if($value == $option2)
            {
                return "selected='selected'";
            }
        }
    }

    public static function selectedArrayPost($name, $option2)
    {
        if(is_array($_POST[$name]))
        {
            if(in_array($option2, $_POST[$name]))
                return "selected='selected'";
        }
        else
        if($name = $option2)
            return "selected='selected'";
    }

    public static function selectedArrayGet($name, $option2)
    {
        if(isset($_GET[$name]) && is_array($_GET[$name]))
        {
            if(in_array($option2, $_GET[$name]))
                return "selected='selected'";
        }
    }

    public static function selectedArray($option1, $option2)
    {
        if(is_array($option2))
        {
            if(in_array($option1, $option2))
                return "selected='selected'";
        }
        else
        if($option1 == $option2)
        {
            return "selected='selected'";
        }
    }

    public static function selected($option1, $option2)
    {
        if(is_array($option2))
        {
            if(in_array($option1, $option2))
                return "selected='selected'";
        }
        else
        if($option1 == $option2)
        {
            return "selected='selected'";
        }
    }

    public static function checkedPost($name, $value, $option2)
    {
        if(isset($_POST[$name]))
        {
            if($_POST[$name] == $option2)
            {
                return "checked='checked'";
            }
        }
        else
        {
            if($value == $option2)
            {
                return "checked='checked'";
            }
        }
    }

    public static function checkedGet($name, $value, $option2)
    {
        if(isset($_GET[$name]))
        {
            if($_GET[$name] == $option2)
            {
                return "checked='checked'";
            }
        }
        else
        {
            if($value == $option2)
            {
                return "checked='checked'";
            }
        }
    }

    public static function checked($option1, $option2)
    {
        if($option1 == $option2)
        {
            return "checked='checked'";
        }
    }

    public static function genrateRegistartionCode()
    {
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789!@';
        $string = '';
        for($i = 0; $i < 9; $i++)
        {
            $string .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $string;
    }

    public static function createThumb($path, $image, $dest, $width = 150, $height = 150)
    {
        $img = \Intervention\Image\Facades\Image::make($path . '/' . $image)->resize($width, $height);
        $img->save($dest . '/' . $image);
        return $dest . '/' . $image;
    }

    public static function makeThumb($src, $dest, $desired_width, $desired_height = 'auto')
    {
        //if (!file_exists($src))
        //    return false ;
        // image type
        $ext = exif_imagetype($src);

        /* read the source image */
        //if($ext!="JPG" || $ext!="JPEG" || $ext!="jpg")
        if($ext == '1') //GIF
            $source_image = @imagecreatefromgif($src);
        elseif($ext == "2") //jpg
            $source_image = @imagecreatefromjpeg($src);
        elseif($ext == "3") //png
            $source_image = @imagecreatefrompng($src);
        else
            $source_image = $src;
        $width = @imagesx($source_image);
        $height = @imagesy($source_image);
        if($desired_height == 'auto')
        {
            /* find the "desired height" of this thumbnail, relative to the desired width  */
            $desired_height = @floor($height * ($desired_width / $width));
        }

        /* create a new, "virtual" image */
        $virtual_image = @imagecreatetruecolor($desired_width, $desired_height);
        /* copy source image at a resized size */
        @imagecopyresized($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
        /* create the physical thumbnail image to its destination */
        @imagejpeg($virtual_image, $dest);
    }

    public static function issetPost($name, $value)
    {
        if(isset($_POST[$name]))
        {
            return $_POST[$name];
        }
        else
        {
            return $value;
        }
    }

    public static function issetGet($name, $value)
    {
        if(isset($_GET[$name]))
        {
            return $_GET[$name];
        }
        else
        {
            return $value;
        }
    }

    public static function issetLang($name, $lang_id, $value)
    {
        if(isset($_POST['lang']))
        {
            return $_POST['lang'][$lang_id][$name];
        }
        else
        {
            return $value;
        }
    }

    public static function issetArrayPost($name)
    {
        if(is_array($_POST[$name]))
        {
            foreach($_POST[$name] as $post)
            {
                if(in_array($_POST[$name], $post))
                    return $post;
            }
        }
    }

    public static function calculateTime($time)
    {
        $starttime = $time;
        $stoptime = date('Y-m-d H:i:s');
        $diff = (strtotime($stoptime) - strtotime($starttime));
        $total = $diff / 60;
        return $result_hours = sprintf("%02dH %02dM", floor($total / 60), $total % 60);
    }

    public static function calculateDate($date)
    {
        $timestamp = strtotime($date);
        $datetime1 = date_create(date('Y-m-d', $timestamp));
        $datetime2 = date_create(date('Y-m-d'));
        $interval = date_diff($datetime1, $datetime2);
        return $interval->format('%a Days');
    }

    public static function createWaterMark($pathImage, $pathLogo = '', $pathSave, $width = null, $height = null)
    {
        $img = \Intervention\Image\Facades\Image::make($pathImage);

        if($width != null && $height != null)
            $img->resize($width, $height);

        if($pathLogo != '')
            $img->insert($pathLogo, 'bottom-left', 10, 10);

        $img->save($pathSave);
    }

    public static function resizeImage($pathImage, $pathSave, $width = null, $height = null)
    {
        $img = \Intervention\Image\Facades\Image::make($pathImage);

        if($width != null && $height != null)
            $img->resize($width, $height);

        $img->save($pathSave);
    }

    public static function viewImageBySize($pathImage, $width)
    {
        $img = \Intervention\Image\Facades\Image::make($pathImage);

        $img->fit(300, 200);

        return $img->save($pathImage);
    }

    public static function viewImageHeight($pathImage)
    {
        $img = \Intervention\Image\Facades\Image::make($pathImage)->height();

        return $img;
    }

    public static function viewImageWidth($pathImage)
    {
        $img = \Intervention\Image\Facades\Image::make($pathImage)->width();

        return $img;
    }

    public static function sendEmail($fromName, $from, $to, $subject, $message, $cc = '', $bcc = '')
    {
        $eol = PHP_EOL;
        $headers = "MIME-Version: 1.0" .$eol;
        $headers .= "Content-type:text/html;charset=UTF-8" .$eol;
        $headers .= header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        $headers .= header("Cache-Control: post-check=0, pre-check=0", false);
        $headers .= header("Pragma: no-cache");
//        $headers .= 'To: <' . $to . '>' . "\r\n";
        $headers .= 'From: ' . $fromName . ' <' . $from . '>' .$eol;
        if($cc != '')
            $headers .= 'Cc: ' . $cc .$eol;
        if($bcc != '')
            $headers .= 'Bcc: ' . $bcc .$eol;

        $email = mail($to, $subject, $message, $headers, '-freturn@domain');

        return $email;
    }

    public static function getCountry()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $jsonData = file_get_contents('http://freegeoip.net/json/' . $ip);
        $countryInfo = json_decode($jsonData, true);
        return $countryInfo;
    }

    public static function getCountryByIP($ip)
    {
        $jsonData = file_get_contents('http://freegeoip.net/json/' . $ip);
        $countryInfo = json_decode($jsonData, true);
        return $countryInfo;
    }

    public static function getCountryIP()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $jsonData = file_get_contents('http://freegeoip.net/json/' . $ip);
        $countryInfo = json_decode($jsonData, true);
        return $countryInfo['ip'];
    }

    public static function getCountrySymbol()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $jsonData = file_get_contents('http://freegeoip.net/json/' . $ip);
        $countryInfo = json_decode($jsonData, true);
        return $countryInfo['country_code'];
    }

    public static function getCountryName()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $jsonData = file_get_contents('http://freegeoip.net/json/' . $ip);
        $countryInfo = json_decode($jsonData, true);
        return $countryInfo['country_name'];
    }

    public static function detectDevice($userAgent)
    {
        $devicesTypes = array(
            "computer" => array("msie 10", "msie 9", "msie 8", "windows.*firefox", "windows.*chrome", "x11.*chrome", "x11.*firefox", "macintosh.*chrome", "macintosh.*firefox", "opera"),
            "tablet" => array("tablet", "android", "ipad", "tablet.*firefox"),
            "mobile" => array("mobile ", "android.*mobile", "iphone", "ipod", "opera mobi", "opera mini"),
            "bot" => array("googlebot", "mediapartners-google", "adsbot-google", "duckduckbot", "msnbot", "bingbot", "ask", "facebook", "yahoo", "addthis")
        );
        foreach($devicesTypes as $deviceType => $devices)
        {
            foreach($devices as $device)
            {
                if(preg_match("/" . $device . "/i", $userAgent))
                {
                    $deviceName = $deviceType;
                }
            }
        }
        return ucfirst($deviceName);
    }

    public static function getBrowser($userAgent)
    {
        if(strpos($userAgent, 'MSIE') !== FALSE)
            return 'Internet explorer';
        elseif(strpos($userAgent, 'Firefox') !== FALSE)
            return 'Mozilla Firefox';

        elseif(strpos($userAgent, 'Chrome') !== FALSE)
            return 'Google Chrome';
        else
            return 'Something else';
    }

    public static function getLatLong($address)
    {
        preg_match('#@(\d+.\d+),(\d+.\d+)#', $_POST['map'], $latlng);

        $lat = $latlng[1];
        $long = $latlng[2];

        return [$lat, $long];
    }

    public static function getYoutubeStream($url, $width = 200, $height = 100)
    {
        return'<iframe width="' . $width . '" height="' . $height . '" src="' . $url . '" frameborder="0" allowfullscreen></iframe>';
    }

    public static function getYoutubeStreamLink($url)
    {
        $id = self::getYoutubeId($url);
        $stream = "https://www.youtube.com/embed/" . $id;
        return $stream;
    }

    public static function getYoutubeImg($url)
    {
        $id = self::getYoutubeId($url);
        $img = "http://img.youtube.com/vi/" . $id . "/0.jpg";
        return $img;
    }

    public static function getYoutubeId($url)
    {
        parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
        return $my_array_of_vars['v'];
    }

    public static function selectedArrayDisabled($option1, $option2)
    {
        if(is_array($option2))
        {
            if(in_array($option1, $option2))
                return "disabled";
        }
        else
        if($option1 == $option2)
        {
            return "disable";
        }
    }

    public static function fileToArray(\SplFileInfo $file)
    {
        if(!$file->isFile())
            return [];
        $file_arr = null;
        $file_syntax = shell_exec('php -l ' . $file->getPathname());
        if(strpos($file_syntax, 'No syntax errors detected') === 0)
        {
            $file_arr = \File::getRequire($file->getPathname());
        }
        return $file_arr;
    }

    public static function MultiArrayToArray($array, $ret = array(), $index = '')
    {
        foreach($array as $key => $value)
        {
            if(is_array($value))
            {
                $index .= $key . '.';
                $ret = self::MultiArrayToArray($value, $ret, $index);
                $index = '';
            }
            else
            {
                $ret[$index . $key] = $value;
            }
        }
        return $ret;
    }

    public static function ArrayToMultiArray($array, $ret = array(), $index = '')
    {
        $arr_i = 0;
        foreach($array as $key => $value)
        {
            $arr_k = array();
            if(strpos($key, '.') > 0)
            {
                $arr_key = explode('.', $key);
                $arr_key = array_reverse($arr_key);
                $main_key = array_pop($arr_key);
                $arr_k[$arr_i] = &$ret[$main_key];

                $arr_key = array_reverse($arr_key);
                foreach($arr_key as $i => $index)
                {
                    $arr_k[$arr_i][$index] = isset($arr_k[$arr_i][$index]) ?
                            $arr_k[$arr_i][$index] : array();
                    $arr_k[$arr_i] = &$arr_k[$arr_i][$index];
                }
                $arr_k[$arr_i] = $value;
                $i++;
            }
            else
            {
                $ret[$key] = $value;
            }
        }
        return $ret;
    }

    public static function isMobile()
    {
        if(!isset($_SERVER['HTTP_USER_AGENT']))
        {
            return false;
        }
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4)))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public static function getCategoriesSelectBox($selected = '', $required = '')
    {
        $html = '<label class="control-label">Categories</label>'
                . '<select name="category" class="form-control select2clear" ' . $required . '>'
                . '<option selected disabled>Please Choose</option>'
                . '<option ' . Functions::selected('Fine Arts', $selected) . ' value="Fine Arts">Fine Arts</option>'
                . '<option ' . Functions::selected('Illustration', $selected) . ' value="Illustration">Illustration</option>'
                . '<option ' . Functions::selected('Sketches', $selected) . ' value="Sketches">Sketches</option>';
        $html.='</select>';
        return $html;
    }

    static function arabicDate($date)
    {
        $arabic_months = [
            1 => 'يناير',
            2 => 'فبراير',
            3 => 'مارس',
            4 => 'ابرايل',
            5 => 'مايو',
            6 => 'يونيو',
            7 => 'يوليو',
            8 => 'اغسطس',
            9 => 'سبتمبر',
            10 => 'اكتوبر',
            11 => 'نوفمبر',
            12 => 'ديسمبر',
        ];

        $date_parts = explode('-', $date);

        return $date_parts[0] . ' ' . $arabic_months[$date_parts[1]] . ' ' . $date_parts[2];
    }

    public static function getYoutubeEmbedFromUrl($url)
    {
        return preg_replace(
                "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "<iframe src=\"https://www.youtube.com/embed/$2\" rameborder='0' allowfullscreen></iframe>", $url
        );
    }

    public static function isAdmin(){
        if(session('backendUser') == null || session('role') == null)
            return false ;
        return true ;
    }

    public static function issetOld($name, $value)
    {
        if (old($name)) {
            return old($name);
        } else {
            return $value;
        }
    }
	
	public static function local(){
		$whitelist = array( '127.0.0.1', '::1' );
		return in_array( $_SERVER['REMOTE_ADDR'], $whitelist);
	}
}
