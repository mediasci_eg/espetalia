<?php
namespace App\Http\Controllers\Helpers ;
class Youtube {
    
    static function stream($url , $width=200 , $height=100)
    {
        return'<iframe width="'.$width.'" height="'.$height.'" src="'.$url.'" frameborder="0" allowfullscreen></iframe>';
    }
    
    static function streamLink($url)
    {
        $id = self::getId($url) ;
        $stream= "https://www.youtube.com/embed/".$id;
        return $stream;
    }
    
    static function img($url)
    {
        $id = self::getId($url) ;
        $img = "http://img.youtube.com/vi/".$id."/0.jpg";
        return $img ;
    }
    
    static function getId($url)
    {
        parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
        return $my_array_of_vars['v'];   
    }
}

