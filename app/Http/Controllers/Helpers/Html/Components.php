<?php

namespace App\Http\Controllers\Helpers\Html;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use Intervention\Image\Image;
use Intervention\Image\Facades\Image;

class Components extends Controller
{

    public static function uploader($image = './assets/images/avatar_image.png', $css_classes = '')
    {

        if($image == '' || !file_exists($image))
            $image = './assets/images/avatar_image.png';
        $html = file_get_contents(__DIR__ . '/uploader.html');
        $html = str_replace('profile_picture', $image, $html);
        $html = str_replace('{css_classes}', $css_classes, $html);

        return $html;
    }

    public static function addGoogleMap()
    {
        $html = file_get_contents(__DIR__ . "/googlemap.html");
        return $html;
    }

    public static function addGoogleMapShow($lat, $lng)
    {
        if($lat==''){
            $lat=28.07198030177986;
        }
        if($lng==''){
            $lng=30.9814453125;
        }
        $html = file_get_contents(__DIR__ . "/googlemapshow.html");
        $html = str_replace("{lat}", $lat, $html);
        $html = str_replace('{lng}', $lng, $html);
        return $html;
    }

    public static function editGoogleMap($lat, $lng, $zoom=16)
    {
        if($lat==''){
            $lat=28.07198030177986;
        }
        if($lng==''){
            $lng=30.9814453125;
        }
        if($zoom==''){
            $zoom=16;
        }
        $html = file_get_contents(__DIR__ . "/googlemapedit.html");
        $html = str_replace("{lat}", $lat, $html);
        $html = str_replace('{lng}', $lng, $html);
        //$html = str_replace('{img}', $img, $html);
        //$html = str_replace('{desc}', $desc, $html);
        $html = str_replace('{zoom}', $zoom, $html);
        return $html;
    }

    public static function showGoogleMap($lat, $lng, $zoom)
    {
        $html = file_get_contents(__DIR__ . "/showgooglemap.html");
        $html = str_replace("{lat}", $lat, $html);
        $html = str_replace('{lng}', $lng, $html);
        //$html = str_replace('{img}', $img, $html);
        //$html = str_replace('{desc}', $desc, $html);
        $html = str_replace('{zoom}', $zoom, $html);
        return $html;
    }

    public static function showGoogleMapMultiple($result, $urlImage)
    {
        $html = file_get_contents(__DIR__ . "/showgooglemapmultiple.html");
        $html = str_replace('{result}', $result, $html);
        $html = str_replace('{url_image}', $urlImage, $html);
        return $html;
    }

    public static function uploadMultiple($urlUpload, $savePath, $result, $urlDelete)
    {
        $html = file_get_contents(__DIR__ . "/upload_multiple.html");
        $html = str_replace('{urlUpload}', $urlUpload, $html);
        $html = str_replace('{savePath}', $savePath, $html);
        $html = str_replace('{result}', $result, $html);
        $html = str_replace('{urlDelete}', $urlDelete, $html);
        return $html;
    }

    public static function scriptTableLang($url)
    {
        $html = file_get_contents(__DIR__ . "/script_table_lang.html");
        $html = str_replace('{url}', $url, $html);
        return $html;
    }
    
    public static function media($name,$value = '') {
		$html = file_get_contents(__DIR__ . "/media.html");
                $html = str_replace("{name}", $name, $html);
		if ($value != '') {
			$html = str_replace("{value}", $value, $html);
		} else {
			$html = str_replace("{value}", '', $html);
		}
		return $html;
	}

}
