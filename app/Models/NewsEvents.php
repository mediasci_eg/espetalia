<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsEvents extends BaseModel {

    protected $table = 'news_events';
    protected $fillable = ['title', 'images', 'link', 'content', 'slug', 'is_featured', 'date'];
    protected $casts = [
        'images' => 'array',
    ];
    public $rules = [
        'title' => "required",
        'content' => "required",
        'slug' => "required",
        'date' => "required",
    ];

}
