<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends BaseModel
{
    protected $casts = [
        'data' => 'array',
         'images' => 'array',
    ];


    public function images(){
        return $this->hasMany('App\Models\postsImages','post_id');
    }
}
