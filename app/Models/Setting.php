<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends BaseModel
{
    //
    public $timestamps = false;
    protected $fillable = ['value', 'order', 'type', 'key'];
    public $rules = [
        'value' => 'required',
        'order' => 'required',
        'type' => 'required',
        'key' => 'required'
    ];
}
