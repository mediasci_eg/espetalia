<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonials extends BaseModel
{
    //
     protected $fillable = ['name', 'content'];
    public $rules = [
        'name' => 'required',
        'content' => 'required',
    ];
}
