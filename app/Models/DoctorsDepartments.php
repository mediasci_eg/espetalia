<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorsDepartments extends BaseModel
{
    //
     protected $fillable = ['doctor_id', 'department_id'];
    public $rules = [
        'doctor_id' => 'required',
        'department_id' => 'required',
    ];
}
