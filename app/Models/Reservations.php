<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservations extends BaseModel {

    //
    use SoftDeletes;

    protected $fillable = ['name', 'phone', 'email', 'doctor_id', 'message', 'date', 'time'];
    public $rules = [
        'name' => 'required',
        'phone' => 'required',
        'email' => 'required',
        'doctor_id' => 'required',
        'date' => 'required',
        'time' => 'required',
        'message' => 'required',
    ];

    public function doctor() {
        return $this->belongsTo(Doctor::class, 'doctor_id');
    }

}
