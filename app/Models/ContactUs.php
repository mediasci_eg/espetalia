<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;

class ContactUs extends BaseModel {

    //
    use SoftDeletes;
    protected $fillable = ['name', 'mail', 'phone', 'subject', 'message'];
    public $rules = [
        'name' => 'required',
        'mail' => 'required',
        'phone' => 'required',
        'subject' => 'required',
        'message' => 'required',
    ];

}
