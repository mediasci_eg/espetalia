<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends BaseModel
{
    //
     protected $fillable = ['name', 'content', 'short_content', 'image'];
    public $rules = [
        'name' => 'required',
        'content' => 'required',
        'short_content' => 'required',
        'image' => 'required',
    ];
}
