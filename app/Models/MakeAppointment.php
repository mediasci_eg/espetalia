<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MakeAppointment extends BaseModel {

    //
    use SoftDeletes;
    protected $fillable = ['department_id', 'full_name', 'phone', 'date'];
    public $rules = [
        'department_id' => 'required',
        'full_name' => 'required',
        'phone' => 'required',
        'date' => 'required',
    ];

    public function department() {
        return $this->belongsTo(Department::class, 'department_id');
    }

}
