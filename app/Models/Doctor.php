<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doctor extends BaseModel {

    //
    protected $fillable = ['name', 'content', 'image', 'title'];
    public $rules = [
        'name' => 'required',
        'title' => 'required',
//        'department_id' => 'required',
        'content' => 'required',
        'image' => 'required',
    ];

//    public function department() {
//        return $this->belongsTo(Department::class, 'department_id');
//    }

}
