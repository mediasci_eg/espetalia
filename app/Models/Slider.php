<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends BaseModel {

    //
    protected $table = 'sliders';
    protected $fillable = ['name', 'content', 'image', 'link'];
    public $rules = [
        'name' => 'required',
        'image' => 'required',
    ];

}
