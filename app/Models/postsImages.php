<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class postsImages extends Model
{
    protected $table = 'posts_images';
    protected $fillable = ['post_id', 'image'];
    public $timestamps = false;
    public $rules = [
        'post_id' => "required",
        'image' => "required"
    ];

    public function post(){
        return $this->belongsTo('App\Models\Post','post_id');
    }
}
