<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Apply extends BaseModel
{
    protected $table = 'apply';
    public $timestamps = true;
    protected $fillable = ['first_name', 'last_name', 'email', 'phone', 'gender',
        'date_birth', 'national_id', 'address', 'emergency_number',
         'nationality', 'governerate'
        , 'type_of_certificate', 'other_certificate', 'awarded_certificate_year'
        , 'school_institution', 'programme',
        'occupation', 'relationship', 'country','guardian_name','full_address'
    ];
    public $ignored_unique = [
//        'email',
//        'phone',
    ];
    public $rules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => "required|email",
        'phone' => "required",
        ///
        'gender' => 'required',
        'date_birth' => 'required',
        'national_id' => "required",
        'address' => "required",
        'full_address' => "required",
        ///
        'emergency_number' => 'required',
        'nationality' => 'required',
        'governerate' => "required",
        'type_of_certificate' => "required",
        ///
        'other_certificate' => 'required',

        'school_institution' => "required",
        ///
        'programme' => 'required',
        'occupation' => 'required',
        'relationship' => "required",
        'country' => "required",
        'guardian_name' => "required",
    ];

    protected $guarded = ['id'];
}
